<?php $__env->startSection('content'); ?>

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">ABOUT US</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30">
    <div class="container">
     
<?php $__currentLoopData = $about_content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
   <div class="col-md-12 contact wow fadeInUp">      
         <?php echo $value->content; ?>

        </div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> 
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('specific_js'); ?>
<!-- components-main.js -->
<script type="text/javascript"  src="<?php echo e(url('/')); ?>/website_assets/js/customs/components-main.js"></script> 
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>