<?php $__env->startSection('content'); ?>	
<div class="clearfix"></div>

<!-- Page Header End -->
<div class="row">
    <div class="container">

        <!-- LEFT SIDE BEGIN           
        ===============================================================-->	
        <div class="col-md-8">

            <!-- SINGLE POST BEGIN           
            ===============================================================-->	
            <div class="row">

                <div class="col-sm-12 col-md-12">  

                    <div class="thumbnail text-left">

                        <div class="caption" style="margin-top:10px">

                            <div class="post-category">          
                                <ul>
                                    <li><a href="#">Travel</a></li>
                                    <li><a href="#">Fashion</a></li>
                                    <li><a href="#">Art</a></li>             
                                </ul>          
                            </div>

                            <h2 class="plist-header-bold text-uppercase mrb-20" style="margin-top:15px">Post Type : Link Post
                            </h2>

                            <p class="post-bottom-title mrt-20">
                                <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus justo id eros tincidunt feugiat. Morbi neque nulla, luctus quis quam eu, elementum sollicitudin sem.</b>
                            </p>

                            <p>
                                Sed elementum ante a euismod pulvinar. Ut varius neque vitae felis maximus, ut finibus neque cursus. Proin rutrum massa vel molestie feugiat. Ut malesuada magna sapien, sit amet condimentum tortor suscipit eu. Nullam lacinia tellus odio, quis ultrices odio sagittis eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus sagittis luctus sagittis. Nam luctus vitae mauris eget lobortis. <b>Cras mollis neque</b> ac tempor ultrices. Cras aliquet aliquam nunc, ut tempor sapien porta sit amet. Duis pellentesque in sem a bibendum.
                            </p>

                            <blockquote>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Suspendisse neque dolor, pellentesque sit amet efficitur sit amet
                                </p>
                                <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                            </blockquote>

                            <p>
                                Suspendisse nulla lectus, blandit id turpis vel, dictum luctus eros. Ut venenatis eleifend vestibulum. Maecenas eget bibendum diam, eu euismod tellus. Vivamus eget urna velit. Pellentesque a consequat elit. Praesent ullamcorper eu ipsum vel convallis. Aliquam non scelerisque lectus. Vestibulum ut mollis elit. 
                            </p>

                            <p>
                                Donec nec semper eros. <b>Cum sociis</b> natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque sagittis metus sed metus vestibulum, vel facilisis tortor tempor. Aenean condimentum id turpis a efficitur. Integer sit amet accumsan sem. Mauris urna lorem, feugiat id mollis vitae, euismod at ex. Phasellus dapibus at ante id sodales.
                            </p>

                            <hr />     
                            <p class="post-date-author-comment pull-right expand">
                                JULY 12ND, 2015 - BY <a href="#">TUTEN</a> - <a href="#"><i class="flaticon-comment33"></i> 3 Comment</a>
                            </p>



                            <div class="social-icons pull-left expand" style="display:inline-block">
                                <ul>

                                    <li data-toggle="tooltip" data-placement="top" title="Facebook">
                                        <a href="#" class="fb-icon"><i class="fa fa-facebook"></i></a>
                                    </li>

                                    <li data-toggle="tooltip" data-placement="top" title="Twitter">
                                        <a href="#" class="twitter-icon"><i class="fa fa-twitter"></i></a>
                                    </li>

                                    <li data-toggle="tooltip" data-placement="top" title="Google Plus">
                                        <a href="#" class="google-icon"><i class="fa fa-google-plus"></i></a>
                                    </li>

                                    <li data-toggle="tooltip" data-placement="top" title="Linkedin">
                                        <a href="#" class="linkedin-icon"><i class="fa fa-linkedin"></i></a>
                                    </li>

                                    <li data-toggle="tooltip" data-placement="top" title="Rss">
                                        <a href="#" class="rss-icon"><i class="fa fa-rss"></i></a>
                                    </li>

                                </ul>
                            </div> 

                            <div class="clearfix"></div> 

                        </div>
                    </div>

                </div>  

                <div class="clearfix"></div> 

            </div> <!--row-->  


         
            <!-- ABOUT THE AUTHOR END           
            ===============================================================-->	

            <!-- RELATED POSTS BEGIN					
            ===============================================================-->
            <h3 class="header-title-center">Related Posts</h3>

            <div id="owl-carousel">

                <!-- Carousel Item 1 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/1.jpg" alt="Owl Image"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/1.jpg">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                            <ul>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Art</a></li>             
                            </ul>          
                        </div>

                        <h4 class="plist-header-bold"><a href="#">Lorem Ipsum Dolor Sit Amet</a></h4>
                    </div>  
                </div>

                <!-- Carousel Item 2 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/2.jpg" alt="Owl Image"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/2.jpg">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                            <ul>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Art</a></li>             
                            </ul>          
                        </div>

                        <h4 class="plist-header-bold"><a href="#">Lorem Ipsum Dolor Sit Amet</a></h4>
                    </div>  
                </div>

                <!-- Carousel Item 3 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/3.jpg" alt="Owl Image"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/3.jpg">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                            <ul>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Art</a></li>             
                            </ul>          
                        </div>

                        <h4 class="plist-header-bold"><a href="#">Lorem Ipsum Dolor Sit Amet</a></h4>
                    </div>  
                </div>

                <!-- Carousel Item 4 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/4.jpg" alt="Owl Image"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/4.jpg">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                            <ul>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Art</a></li>             
                            </ul>          
                        </div>

                        <h4 class="plist-header-bold"><a href="#">Lorem Ipsum Dolor Sit Amet</a></h4>
                    </div>  
                </div>

                <!-- Carousel Item 5 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/5.jpg" alt="Owl Image"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/5.jpg">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                            <ul>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Art</a></li>             
                            </ul>          
                        </div>

                        <h4 class="plist-header-bold"><a href="#">Lorem Ipsum Dolor Sit Amet</a></h4>
                    </div>  
                </div>			  			   
            </div><!-- owl-carousel-->
            <!-- RELATED POSTS END					
            ===============================================================-->


       

            <!-- COMMENTS END					
            ===============================================================-->

        </div>
        <!-- LEFT SIDE END           
        ===============================================================-->

        <!-- SIDEBAR BEGIN           
        ===============================================================-->
        <div class="col-md-4 Sidebar">		 
            <div class="theiaStickySidebar"> 				
                <div class="row"> 


                    <!-- INSTAGRAM BEGIN           
                    ===============================================================-->
                    <div class="col-sm-12 col-md-12">					  

                        <div class="thumbnail text-center sb-padding-10">	

                            <div class="widget-header-line"></div>							
                            <h3 class="sb-header text-center">INSTAGRAM</h3>								  
                            <div class="widget-header-line"></div>								

                            <!-- LightWidget WIDGET --><script src="http://lightwidget.com/widgets/lightwidget.js"></script><iframe src="http://lightwidget.com/widgets/316d47e8bfbc5abf930b1541b6b46fc1.html" id="lightwidget_316d47e8bf" name="lightwidget_316d47e8bf" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>					   

                        </div>

                    </div> 
                    <!-- INSTAGRAM END           
                    ===============================================================-->               
                </div><!--row-->

            </div><!--Sticky Sidebar-->

        </div><!--md-4 (Sidebar End)-->		
        <!-- SIDEBAR END           
        ===============================================================-->	

    </div><!--container-->
</div>

<div class="clearfix"></div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('specific_js'); ?>
<!-- Sticky Sidebar -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/theia-sticky-sidebar-master/theia-sticky-sidebar.js"></script>
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/theia-sticky-sidebar-master/test.js"></script>
<!-- blog.js -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/customs/blog.js"></script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>