<?php $__env->startSection('content'); ?>
	<div class="clearfix"></div>

		<!-- Page Header Begin -->
		<div class="row">
			<div class="page-header-one">
				<div class="container">
					<div class="col-lg-12">
						<h3 class="pull-left">NEWS AND EVENTS</h3>
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Page Header End -->
            <div class="row">
                <div class="container">
                    <!--POST 1 BEGIN       
                    ===============================================================--> 
                    <?php $__currentLoopData = $news_content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <div class="col-sm-6 col-md-6">

                        <div class="thumbnail text-center">

                            <!--sinister-hover-effects-->
                            <div class="ImageWrapper BackgroundS">
                                <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/blog-1.jpg" alt="..." style="width:100%"/>
                                <div class="ImageOverlayH"></div>
                                <div class="StyleLi">
                                    <span class="WhiteRounded">
                                        <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/blog-1.jpg">
                                            <i class="flaticon-search19"></i>
                                        </a>
                                    </span>
                                    <span class="WhiteRounded">
                                        <a href="#"><i class="flaticon-link15"></i></a>
                                    </span>
                                </div>
                            </div>
                            <!--sinister-hover-effects--> 

                            <div class="caption">


                                <h4 class="plist-header-bold text-uppercase"><a href="#"><?php echo e($value->title); ?></a></h4>

                                <?php echo str_limit($value->content,200); ?>


                                <div class="bottom-line"></div>

                                <div class="social-icons margin-bottom-10">
                                    <ul>
                                        <li data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <a href="#" class="fb-icon"><i class="fa fa-facebook"></i></a>
                                        </li>

                                        <li data-toggle="tooltip" data-placement="top" title="Twitter">
                                            <a href="#" class="twitter-icon"><i class="fa fa-twitter"></i></a>
                                        </li>

                                        <li data-toggle="tooltip" data-placement="top" title="Google Plus">
                                            <a href="#" class="google-icon"><i class="fa fa-google-plus"></i></a>
                                        </li>

                                        <li data-toggle="tooltip" data-placement="top" title="Linkedin">
                                            <a href="#" class="linkedin-icon"><i class="fa fa-linkedin"></i></a>
                                        </li>

                                        <li data-toggle="tooltip" data-placement="top" title="Rss">
                                            <a href="#" class="rss-icon"><i class="fa fa-rss"></i></a>
                                        </li>
                                    </ul>
                                </div> 

                                <p class="text-center">
                                    <a href="<?php echo url('/read_single_news/'); ?>/<?php echo e($value->webcontent_id); ?>">
                                        <span class="btn1 btn1-default">CONTINUE READING</span>
                                    </a>
                                </p>

                            </div>
                        </div>					
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <!--POST 1 END       
                    ===============================================================-->    

                    <div class="clearfix"></div> 

                    <!-- PAGINATION
                    ===============================================================--> 
                    <div class="col-md-12">
                        <div class="pagination-buttons">								
                            <ul>                        
                                <li class="pagination-active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>   
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>   
                            </ul>	
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- PAGINATION END
                    ===============================================================--> 

                    <!-- Related Posts Begin					
                    ===============================================================-->
                    <div class="col-md-12">
                        <h3 class="header-title">Related Posts</h3>

                     <div id="owl-carousel">
                           <?php $__currentLoopData = $news_content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <!-- Carousel Item 1 Begin -->
                            <div class="item">  
                                <div class="owl-thumb">
                                    <!--sinister hover effects begin-->
                                    <div class="ImageWrapper BackgroundS">
                                        <img src="<?php echo e(url('website_assets')); ?>/images/blog/post_images/blog-2.jpg" alt="Owl Image"/>
                                        <div class="ImageOverlayH"></div>
                                        <div class="StyleLi">
                                            <span class="WhiteRounded">
                                                <a class="test-popup-link" href="<?php echo e(url('website_assets')); ?>/images/blog/post_images/blog-2.jpg">
                                                    <i class="flaticon-search19"></i>
                                                </a>
                                            </span>	

                                            <span class="WhiteRounded">
                                                <a href="#"><i class="flaticon-link15"></i></a>
                                            </span>

                                        </div>
                                    </div>
                       <h4 class="plist-header-bold"><a href="<?php echo url('/read_single_news/'); ?>/<?php echo e($value->webcontent_id); ?>"><?php echo e($value->title); ?></a></h4>
                                </div>  
                            </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </div><!-- owl-carousel-->	
                    </div>
                    <!-- Related Posts End -->					

                </div><!--container-->
            </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('specific_js'); ?>                
                <!-- Sticky Sidebar -->
	<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/theia-sticky-sidebar-master/theia-sticky-sidebar.js"></script>
	<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/theia-sticky-sidebar-master/test.js"></script>	
               <!-- blog.js -->
	<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/customs/blog.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>