<?php $__env->startSection('content'); ?>

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">RESERVATION</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->


<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30">
    <div class="container">
        <div class="col-md-12 contact wow fadeInUp">		
            <div class="row">
                <div class="contact-form col-md-7">
                    <h4>Reservation Form</h4>
                    <?php if(session('success')): ?>
                    <div class="alert alert-success alert-dismissible"><?php echo e(session('success')); ?></div>
                    <?php elseif(session('error')): ?>
                           <div class="alert alert-danger alert-dismissible"><?php echo e(session('error')); ?></div>
                           <?php endif; ?>
                    <hr/>
           <div class="contact-form-area2">
                        <?php echo Form::open(['url' => 'reservation', 'method' => 'POST','id'=>'reservation']); ?>


                        <?php echo e(csrf_field()); ?>

                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('firstname', 'Firstname'); ?>

                                <?php echo Form::text('firstname', '', ['class' => 'form-control','required'=>'true']); ?>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">						  
                                <?php echo Form::label('lastname', 'Lastname'); ?>

                                <?php echo Form::text('lastname', '', ['class' => 'form-control','required'=>'true']); ?> </div>
                        </div>

                        <div class="col-md-6">

                            <div  class="form-group">
                                <?php echo Form::label('checkin_date', 'Checkin date'); ?>

                                <?php echo Form::date('checkin_date', '', ['class' => 'form-control','required'=>'true']); ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('checkout_date', 'Checkout date'); ?>

                                <?php echo Form::date('checkout_date', '', ['class' => 'form-control','required'=>'true']); ?>

                            </div>

                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('adult', 'Adult'); ?>

                                <?php echo Form::number('adult', '', ['class' => 'form-control']); ?>

                            </div>

                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('children', 'Children'); ?>

                                <?php echo Form::number('children', '', ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('email', 'Email'); ?>

                                <?php echo Form::email('email', '', ['class' => 'form-control','required'=>'true']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  class="form-group">
                                <?php echo Form::label('phonenumber', 'Phonenumber'); ?>

                                <?php echo Form::text('phonenumber', '', ['class' => 'form-control','required'=>'true']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div  class="form-group">
                                <?php echo Form::label('other', 'Other Requirements'); ?>

                                <?php echo Form::textarea('other', '', ['class' => 'form-control','rows' => 2]); ?>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <?php echo Form::submit('SUBMIT'); ?>

                        </div>
                        <?php echo Form::close(); ?>

                        <div class="clearfix"></div>				   

                    </div>	
                </div>


                <div class="contact-info col-md-5">
                    <h4>Reservation important Info</h4>
                    <hr/>			

                    <p>
                       BNN assures you to give you service tha you will enjoy and be glad
                    </p>



                </div>

            </div>

            <div class="clearfix"></div>	
        </div>	
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('specific_js'); ?>
<!-- components-main.js -->
<script type="text/javascript"  src="<?php echo e(url('/')); ?>/website_assets/js/customs/components-main.js"></script> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<!--<script  src="<?php echo e(url('/')); ?>/website_assets/assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>-->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>