<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>
    <div class="col-lg-12">
        <!-- MENU BEGIN           
        ===============================================================-->
        <div class="row">
            <div class="bg-white minti-navbar menuzord-dark">
                <div class="container">
                    <div class="col-lg-12">
                        <div id="menuzord" class="menuzord menuzord-responsive padding-0">	

<!--                            <a href="index.html" class="menuzord-brand"><img src="images/logo/logo.png" alt=""/></a>-->
                            <ul class="menuzord-menu menuzord-indented scrollable menuzord-right">
                                <li class="active"><a href="<?php echo url('/home'); ?>">HOME</a>  
                                </li>
                                <li><a href="#">AFFILLIATES</a>
                                    <div class="megamenu">
                                        <div class="megamenu-row">

                                            <div class="col4">
                                                <ul>
                                                    <li><h5>DARES SALAAM</h5></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i>BSG Bureau de change & finance </a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> BANN fashions </a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> Willips insurance </a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> BNN restaurant </a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> BNN lounge</a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> BNN kibada resort </a></li>
                                                </ul>
                                            </div>
                                            <div class="col4">										
                                                <ul>
                                                    <li><h5>MWANZA</h5></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> Lake Bureau de change </a></li>
                                                    <li><a href="<?php echo url('/affilliate'); ?>"><i class="fa fa-angle-right"></i> Willips fashions </a></li>	
                                                </ul>
                                            </div>
                                            <div class="col4">
                                                <ul>
                                                    <li><h5>MTWARA</h5></li>
                                                    <li><a href="photogallery-v1-2-columns.html"><i class="fa fa-angle-right"></i> BNN royal palm hotel </a></li>
                                                </ul>
                                            </div>

                                        </div>      
                                    </div>
                                </li>
                                <li><a href="<?php echo url('/news'); ?>">NEWS&EVENTS</a>                                       
                                </li>	
                                <li><a href="<?php echo url('/reservation'); ?>">RESERVATION</a>
                                </li>
                                <li><a href="<?php echo url('/gallery'); ?>">GALLERY</a></li>	
                                   <li><a href="<?php echo url('/contact'); ?>">CONTACT</a></li>
                                   
                                   <li><a href="<?php echo url('/about'); ?>">ABOUT US</a></li>
                                    <li><a href="<?php echo url('/feedback'); ?>">GIVE FEEDBACK</a></li>
<!--                                <li class="search">
                                    <form action="#your-action-page" method="get">
                                        <input type="text" name="search" placeholder="Search"/>
                                    </form>
                                </li>-->

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MENU END           
        ===============================================================-->

        <div class="clearfix"></div>
	<?php echo $__env->yieldContent('content'); ?>					

       <div class="clearfix"></div>

   <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     
    </div>

    <!-- JAVASCRIPT FILES
    ===============================================================-->
    <!-- Revolution Slider -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/jquery.themepunch.revolution.min.js"></script>	
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

    <!-- Menuzord -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/menuzord.js"></script>
    <!-- Magnific PopUp -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/jquery.magnific-popup.js"></script>
    <!-- OWL Carousel -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/owl.carousel.js"></script>
    <!-- ToTop -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/toTop.js"></script>	
    <!-- Bootstrap.min.js -->
    <script src="<?php echo e(url('/')); ?>/website_assets/js/bootstrap.min.js"></script> 
    <!-- Wow.js -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/wow.min.js"></script> 	

    <?php echo $__env->yieldContent('specific_js'); ?>

</body>

<!-- Mirrored from www.tegthemes.com/Minti/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Aug 2016 15:05:27 GMT -->
</html>


