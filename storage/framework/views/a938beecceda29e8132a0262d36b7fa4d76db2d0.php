<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- STYLESHEETS
    ===============================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/bootstrap-theme.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/style.css"/>
    <link rel="shortcut icon" href="<?php echo e(url('/')); ?>/website_assets/images/bnn_logo.png" type="image/png">

    <!-- Menuzord Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/menuzord.css"/>
    <!-- Fonts Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/fonticons/flaticon.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/ionicons.css" />
    <!-- Revolution Slider Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/revolution/settings.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/revolution/layers.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/revolution/navigation.css"/>
    <!-- OWL Carousel Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/owl-carousel/owl.theme.css"/>
    <!-- Animate Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/animate.css"/>
    <!-- Sinister Hover Effects Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/snister-hover-effects/sinister.css"/>
    <!-- Magnific Popup Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/')); ?>/website_assets/css/magnific-popup.css"/>
    <!-- Google Fonts -->
<!--    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400italic' rel='stylesheet' type='text/css'/>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type='text/css'/>-->

    <!-- JAVASCRIPTS
    ===============================================================-->
    <!-- JQUERY -->
    <script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/jquery.js"></script>

    <title>BNN</title>

</head>