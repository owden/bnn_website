<?php $__env->startSection('content'); ?>
<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">OUR GALLERY</h3>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<div class="row">				
    <div class="container">	

        <div class="col-md-12">		

            <div class="photo-gallery">  

                <!-- Gallery Buttons Begin -->	
<!--                <div class="photogallery-buttons" id="isotope-btn-group">
                    <ul class="tab">
                        <li><a href="#" data-filter="all">All</a></li>
                        <li><a href="#" data-filter="1">Identity</a></li>
                        <li><a href="#" data-filter="2">Web Design</a></li>
                        <li><a href="#" data-filter="3">Graphic</a></li>
                        <li><a href="#" data-filter="4">Logo</a></li>
                    </ul>
                </div>-->
                <!-- Gallery Buttons End -->

                <div class="row">
                    <div class="filtr-3-column filtr-container" style="margin-top:20px">

                        <ul>

                            <li class="filtr-item text-center" data-category="2, 4" data-sort="value">							
                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>

                            </li>

                            <li class="filtr-item text-center" data-category="1, 3, 4" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="2, 3" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 2" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>		

                            </li>

                            <li class="filtr-item text-center" data-category="2, 3" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 3, 4" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="2, 3" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 2, 4" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 2" data-sort="value">	

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 2, 3" data-sort="value">

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                            <li class="filtr-item text-center" data-category="1, 4" data-sort="value">

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>

                            </li>

                            <li class="filtr-item text-center" data-category="2, 3" data-sort="value">

                                <div class="ImageWrapper BackgroundS bordered">
                                    <img src="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="<?php echo e(url('/')); ?>/website_assets/images/gallery/blog-3.jpg" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">BNN Owesome Look</a></h4>	
                                <p>By Arnold Yusto</p>	

                            </li>

                        </ul>

                    </div>
                </div>				
            </div>

            <p class="text-center">
                <a href="#"><span class="btn1 btn1-xl btn1-dark-no-bg">LOAD MORE</span></a>
            </p>

            <div class="clearfix"></div>
        </div>


    </div><!--container-->
</div>

<div class="clearfix"></div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('specific_js'); ?>
<!-- Imagesloaded -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/imagesloaded.pkgd.min.js"></script>
<!-- Filterizr -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/jquery.filterizr.js"></script>
<!-- Magnific PopUp -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/jquery.magnific-popup.js"></script>
<!-- gallery-portfolio.js -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/website_assets/js/customs/gallery-portfolio.js"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>