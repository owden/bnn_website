<?php $__env->startSection('content'); ?>

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">CONTACT US</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30">
    <div class="container">
        <div class="col-md-12 contact wow fadeInUp">		

            <div class="contact-map-area2">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3009.9825493789567!2d28.972548843264313!3d41.02563772630609!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab9e7a7777c43%3A0x4c76cf3dcc8b330b!2sGalata!5e0!3m2!1str!2str!4v1456598765713" height="450" style="border:0;width:100%" allowfullscreen></iframe>							
            </div>


            <div class="row">
                <div class="contact-form col-md-7">
                    <h4>Contact Form</h4>
                               <?php if(session('success')): ?>
                    <div class="alert alert-success alert-dismissible"><?php echo e(session('success')); ?></div>
                    <?php elseif(session('error')): ?>
                           <div class="alert alert-danger alert-dismissible"><?php echo e(session('error')); ?></div>
                           <?php endif; ?>
                    <hr/>

                    <div class="contact-form-area2">

                              <?php echo Form::open(['url' => 'contact_form', 'method' => 'POST','id'=>'contact_us']); ?>


                        <?php echo e(csrf_field()); ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                      <?php echo Form::label('fullname', 'Name *'); ?>

                                <?php echo Form::text('fullname', '', ['class' => 'form-control','required'=>'true']); ?>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">						  
                                   <?php echo Form::label('email', 'Email  *'); ?>

                                <?php echo Form::email('email', '', ['class' => 'form-control','required'=>'true']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                      <?php echo Form::label('subject', 'Subject*'); ?>

                                <?php echo Form::text('subject', '', ['class' => 'form-control','required'=>'true']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                 <?php echo Form::label('comment', 'Your comment...'); ?>

                                <?php echo Form::textarea('other', '', ['class' => 'form-control','rows' => 4]); ?>

                            </div>


                            <div class="col-md-12">
                                <?php echo Form::submit('SEND MESSAGE'); ?>

                            </div>
                          <?php echo Form::close(); ?> 
                    
                        <div class="clearfix"></div>				   

                    </div>	
                </div>

 <?php $__currentLoopData = $contact_content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="contact-info col-md-5">
                    <h4>Contact Info</h4>
                    <hr/>			

               
                
                <?php echo $value->content; ?>

              
                	

                </div>

            </div>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="clearfix"></div>	
        </div>	
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('specific_js'); ?>
<!-- components-main.js -->
<script type="text/javascript"  src="<?php echo e(url('/')); ?>/website_assets/js/customs/components-main.js"></script> 
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>