<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::post('reservation','WebController@save_reservation');
Route::post('contact_form','WebController@save_contact');
Route::post('feedback','WebController@save_feedback');
Route::post('read_feedback','WebController@read_feedback');
Route::post('add_user','WebController@add_user');
Route::post('add_news','WebController@upload_file');
Route::post('add_contact','WebController@add_contact');
Route::post('add_about','WebController@add_about');
Route::post('add_gallery','WebController@upload_file');
Route::post('edit_news','WebController@edit_news');
Route::post('update_affilliate','WebController@update_affilliate');
Route::post('edit_affilliate','WebController@affilliate_toedit');
Route::post('update_about','WebController@update_about');
Route::post('update_contact','WebController@update_contact');
Route::post('update_news','WebController@update_news');
Route::post('subscribe','EmailController@subscribe');
Route::post('/new_affilliate', 'WebController@new_affilliate');
Route::get('/debug', function () {
    return view('debug');
});
Route::get('/menu/{id}', 'WebController@menu')->name("frontend.menu");
Route::get('/rating', 'WebController@rating')->name("rating");

Route::get('/news', 'WebController@news')->name('news');
Route::get('/gallery', 'WebController@gallery')->name('gallery');
Route::get('/contact', 'WebController@web_contact')->name('web_contact');
Route::get('/affilliate/{webpart_id}', 'WebController@affiliate')->name('affiliate');
Route::get('/contact_form', 'WebController@save_contact')->name('contact_form');
Route::get('/about', 'WebController@web_about')->name('web_about');
Route::get('/read_single_news/{id}', 'WebController@read_single_news')->name('read_single_news');
Route::get('/select_affilliate/{action}', 'WebController@select_affilliate')->name('select_affilliate');


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'WebController@index');



Route::group(array('middleware' => 'auth'), function () {
Route::group(array('prefix' => 'admin'), function () {

    Route::get('/add/add_contact',function(){
        return view('admin.add.add_contact');
    });
    Route::get('/add/add_gallery',function(){
        return view('admin.add.add_gallery');
    });
    Route::get('/edit/edit_contact',function(){
        return view('admin.edit.edit_contact');
    });

    Route::get('/add/add_user',function(){
        return view('admin.add.add_user');
    });
    Route::get('/edit/edit_user',function(){
        return view('admin.edit.edit_user');
    });
    Route::get('/add/add_news',function(){
        return view('admin.add.add_news');
    });

                       Route::get('/edit/edit_gallery',function(){
        return view('admin.edit.edit_gallery');
    });
                        Route::get('/add/add_about',function(){
        return view('admin.add.add_about');
    });
                       Route::get('/add/add_affilliate',function(){

        return view('admin.add.add_affilliate');
    });
    Route::get('/this_affilliate',function(){
        return view('admin.edit.edit_affilliate');
    });

    Route::get('/all_affilliate',function(){
        return view('admin.all_affilliate');
    });
    Route::get('/inbox',function(){
        return view('admin.inbox');
    });
    Route::get('/edit/compose',function(){
        return view('admin.edit.compose');
    });
    Route::get('/feedback', 'WebController@admin_feedback')->name('admin_feedback');
    Route::get('/add_user', 'WebController@add_user')->name('add_user');
    Route::get('/add_news', 'WebController@upload_file')->name('add_news');
    Route::get('/add_affilliate', 'WebController@add_affilliate')->name('add_affilliate');
    Route::get('/all_user', 'WebController@all_user')->name('all_user');
    Route::get('/all_gallery/{type}', 'WebController@all_news')->name('all_news');
    Route::get('/all_news/{type}', 'WebController@all_news')->name('all_news');
    Route::get('/edit_news', 'WebController@edit_news')->name('edit_news');
    Route::get('/edit_about', 'WebController@edit_about')->name('edit_about');
    Route::get('/edit_contact', 'WebController@edit_contact')->name('edit_contact');
    Route::get('/edit_single_news/{id}/type/{type}', 'WebController@edit_single_news')->name('edit_single_news');
    Route::get('/select_affilliate/{action}', 'WebController@select_affilliate')->name('select_affilliate');
    Route::get('/this_affilliate/{id}', 'WebController@affilliate_toedit')->name('this_affilliate');
    Route::get('/', 'WebController@admin_dashboard')->name('dashboard');
 
    
});
});