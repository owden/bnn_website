<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- STYLESHEETS
    ===============================================================-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/bootstrap-theme.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/custom_style.css"/>
    <link rel="shortcut icon" href="{{url('/')}}/website_assets/images/bnn_logo.png" type="image/png">

    <!-- Menuzord Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/menuzord.css"/>
    <!-- Fonts Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/fonticons/flaticon.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/ionicons.css" />
    <!-- Revolution Slider Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/revolution/settings.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/revolution/layers.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/revolution/navigation.css"/>
    <!-- OWL Carousel Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/owl-carousel/owl.theme.css"/>
    <!-- Animate Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/animate.css"/>
    <!-- Sinister Hover Effects Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/snister-hover-effects/sinister.css"/>
    <!-- Magnific Popup Css -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/magnific-popup.css"/>
 

    <!-- JAVASCRIPTS
    ===============================================================-->
    <!-- JQUERY -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/jquery.js"></script>
     <link rel="stylesheet" href="{{url('/')}}/admin_assets/jalert/jAlert.css" />

    <title>BNN</title>

</head>