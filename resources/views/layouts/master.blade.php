@include('layouts.header')

<body>
    <div class="col-lg-12" >
        <!-- MENU BEGIN           
        ===============================================================-->
        <div class="row" >
            <div class="bg-green_faint minti-navbar menuzord-dark">
                <div class="container">
                    <div class="col-lg-12" >
                        <div id="menuzord" class="menuzord menuzord-responsive padding-0" style='background-color:#20724b ' >	
             <a href="{!! route('frontend.menu', 1) !!}" class="menuzord-brand"><img src="{{url('/')}}/website_assets/images/logo/logo.png" alt=""/></a>
                            <ul class="menuzord-menu menuzord-indented scrollable menuzord-right">

                                @foreach (\App\Models\Webpart::whereRaw("parent_id IS NULL")->get() as $level1)
<li  class="{{ ($level1->webpart_id == 1) ? 'ACTIVE' : '' }}"><a href="{!! route('frontend.menu', $level1->webpart_id) !!}">{!! $level1->name !!}</a>  
    @if (count($level1->webparts))
    <div class="megamenu">
        <div class="megamenu-row">
            @foreach (\App\Models\Webpart::whereRaw("parent_id = $level1->webpart_id")->get() as $level2)
            <div class="col4">
                <ul>
                    <li><h5>{{ $level2->name }}</h5></li>
                    @if (count($level2->webparts))
                    @foreach (\App\Models\Webpart::whereRaw("parent_id = $level2->webpart_id")->get() as $level3)
                    <li><a href="{!! route('frontend.menu', $level3->webpart_id) !!}"><i class="fa fa-angle-right"></i>{{ $level3->name }} </a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</li>
@endforeach
                                                               <li class="search">
                                                                   <a href="{{url('/login')}}">ADMIN</a>
                                                                </li>
                              
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MENU END           
        ===============================================================-->

        <div class="clearfix"></div>
        @yield('content')					

        <div class="clearfix" ></div>

        @include('layouts.footer')

    </div>
    <!-- JAVASCRIPT FILES
    ===============================================================-->
    <!-- Revolution Slider -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/jquery.themepunch.revolution.min.js"></script>	
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

    <!-- Menuzord -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/menuzord.js"></script>
    <!-- Magnific PopUp -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/jquery.magnific-popup.js"></script>
    <!-- OWL Carousel -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/owl.carousel.js"></script>
    <!-- ToTop -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/toTop.js"></script>	
    <!-- Bootstrap.min.js -->
    <script src="{{url('/')}}/website_assets/js/bootstrap.min.js"></script> 
    <!-- Wow.js -->
    <script type="text/javascript" src="{{url('/')}}/website_assets/js/wow.min.js"></script>
    
    <script src="{{url('/')}}/admin_assets/jalert/jAlert.min.js"></script>
    <script src="{{url('/')}}/admin_assets/jalert/jAlert-functions.min.js"> //optional!!</script>
<!--    store cookie-->
    <script src="{{url('/')}}/website_assets/rating/js.cookie.js"></script>    

    @yield('specific_js')

</body>


</html>


