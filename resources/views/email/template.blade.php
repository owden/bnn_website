<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- If you delete this tag, the sky will fall on your head -->
        <meta name="viewport" content="width=device-width" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>BNN Group</title>

        <link rel="stylesheet" type="text/css" href="{{url('/')}}/website_assets/css/email.css" />

    </head>

    <body bgcolor="#FFFFFF">
  <!-- BODY -->
        <table class="body-wrap">
            <tr>
                <td></td>
                <td class="container" bgcolor="#FFFFFF">

               
                        <table>
                            
                            {!!$content!!}
                          
                        </table><!-- /social & contact -->


                                </td>
                            </tr>
                        </table>
                   

                </td>
                <td></td>
            </tr>
        </table><!-- /BODY -->

    </body>
</html>