@foreach (\App\Models\Webpart::whereRaw("parent_id IS NULL")->get() as $level1)
<li class="{{ ($level1->webpart_id == 1) ? 'ACTIVE' : '' }}"><a href="{!! route('frontend.menu', $level1->webpart_id) !!}">{!! $level1->name !!}</a>  
    @if (count($level1->webparts))
    <div class="megamenu">
        <div class="megamenu-row">
            @foreach (\App\Models\Webpart::whereRaw("parent_id = $level1->webpart_id")->get() as $level2)
            <div class="col4">
                <ul>
                    <li><h5>{{ $level2->name }}</h5></li>
                    @if (count($level2->webparts))
                    @foreach (\App\Models\Webpart::whereRaw("parent_id = $level2->webpart_id")->get() as $level3)
                    <li><a href="{!! route('frontend.menu', $level3->webpart_id) !!}"><i class="fa fa-angle-right"></i>{{ $level3->name }} </a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</li>
@endforeach