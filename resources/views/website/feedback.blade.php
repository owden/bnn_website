<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

@extends('layouts.master')
@section('content')              


<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">GIVE US YOUR FEEDBACK</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30">
    <div class="container" style="margin: 30px auto">
        <div class="col-md-8 contact">	




            <div class="contact-form3">
                <h4>Feedback Form</h4>
                <hr/>
                <div class="contact-form-area3 wow fadeInUp">
  @if(session('success'))
                    <div class="alert alert-success alert-dismissible">{{session('success')}}</div>
                    @elseif(session('error'))
                           <div class="alert alert-danger alert-dismissible">{{session('error')}}</div>
                           @endif
                     {!! Form::open(['url' => 'feedback', 'method' => 'POST']) !!}
  <div class="col-md-12">
                            <div class="form-group">						  
                               {!! Form::text('name', '', ['class' => 'form-control','placeholder' => 'Full Name','required'=>'True']) !!}
                             </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">						  
                               {!! Form::email('email', '', ['class' => 'form-control','placeholder' => 'Email Address','required'=>'True']) !!}
                             </div>
                        </div>
                       <div class="col-md-12">
                            <div class="form-group">						  
                               {!! Form::text('phone_number', '', ['class' => 'form-control','placeholder' => 'Phonenumber','required'=>'True']) !!}
                             </div>
                        </div>
                       <div class="col-md-12">
                            <div class="form-group">						  
                               {!! Form::text('country', '', ['class' => 'form-control','placeholder' => 'Country','required'=>'True']) !!}
                             </div>
                        </div>

                        <div class="col-md-12">
                                  {!! Form::textarea('feedback', '', ['class' => 'form-control','rows' => 4,'placeholder'=>'Your feedback here','required'=>'True']) !!}
                     </div>							

                        <div class="col-md-12">
                            {!!Form::submit('SEND MESSAGE')!!}
                        </div>
                        {!! Form::close() !!} 
                    <div class="clearfix"></div>				   

                </div>	
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>

<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>

@stop

@section('specific_js')
<!-- components-main.js -->
<script type="text/javascript"  src="{{url('/')}}/website_assets/js/customs/components-main.js"></script> 
@stop