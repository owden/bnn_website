@extends('layouts.master')
@section('content')
<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">OUR GALLERY</h3>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<div class="row">				
    <div class="container">	

        <div class="col-md-12">		

            <div class="photo-gallery">  

                <!-- Gallery Buttons Begin -->	
<!--                <div class="photogallery-buttons" id="isotope-btn-group">
                    <ul class="tab">
                        <li><a href="#" data-filter="all">All</a></li>
                        <li><a href="#" data-filter="1">Identity</a></li>
                        <li><a href="#" data-filter="2">Web Design</a></li>
                        <li><a href="#" data-filter="3">Graphic</a></li>
                        <li><a href="#" data-filter="4">Logo</a></li>
                    </ul>
                </div>-->
                <!-- Gallery Buttons End -->

                <div class="row">
                    <div class="filtr-3-column filtr-container" style="margin-top:20px">

                        <ul>
   @foreach($all_gallery as $news) 
                            <li class="filtr-item text-center" data-category="2, 4" data-sort="value">
                            
                                
                                <div class="ImageWrapper BackgroundS bordered">
                                
                                    <img src="{{ $news->gallery->file->path }}/{{ $news->gallery->file->name }}" alt="" />
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleC">					
                                        <span class="WhiteRounded">
                                            <a class="gallery-item test-popup-link" href="{{ $news->gallery->file->path }}/{{ $news->gallery->file->name }}" title="Lorem Ipsum"><i class="flaticon-search19"></i></a>
                                        </span>							
                                    </div>
                                </div>				
                                <h4 class="gallery-under-header"><a href="#">{{ $news->title }}</a></h4>	
                               <p>                </p>

                            </li>
                            @endforeach()


                        </ul>

                    </div>
                </div>				
            </div>

            <p class="text-center">
                <!-- PAGINATION
				===============================================================--> 
			       
						
                                                    {!!$all_gallery->links();!!}
                             <!-- PAGINATION END
				===============================================================--> 
<!--                <a href="#"><span class="btn1 btn1-xl btn1-dark-no-bg">LOAD MORE</span></a>-->
            </p>

            <div class="clearfix"></div>
        </div>


    </div><!--container-->
</div>

<div class="clearfix"></div>
</div>
@stop('content')
@section('specific_js')
<!-- Imagesloaded -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/imagesloaded.pkgd.min.js"></script>
<!-- Filterizr -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/jquery.filterizr.js"></script>
<!-- Magnific PopUp -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/jquery.magnific-popup.js"></script>
<!-- gallery-portfolio.js -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/customs/gallery-portfolio.js"></script> 
@stop