@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">CONTACT US</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30" style="back">
    <div class="container">
        <div class="col-md-12 contact wow fadeInUp">		

            <div class="contact-map-area2">
<!--                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3009.9825493789567!2d28.972548843264313!3d41.02563772630609!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab9e7a7777c43%3A0x4c76cf3dcc8b330b!2sGalata!5e0!3m2!1str!2str!4v1456598765713" height="450" style="border:0;width:100%" allowfullscreen></iframe>							-->
            </div>


            <div class="row">
                <div class="contact-form3 col-md-7">
                    <h4>Contact Form</h4>
                               @if(session('success'))
                    <div class="alert alert-success alert-dismissible">{{session('success')}}</div>
                    @elseif(session('error'))
                           <div class="alert alert-danger alert-dismissible">{{session('error')}}</div>
                           @endif
                    <hr/>

                    <div class="contact-form-area2">

                              {!! Form::open(['url' => 'feedback', 'method' => 'POST','id'=>'contact_us']) !!}

                        {{csrf_field()}}
                            <div class="col-md-6">
                                <div class="form-group">
                                      {!! Form::label('fullname', 'Name *') !!}
                                {!! Form::text('name', '', ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">						  
                                   {!! Form::label('email', 'Email  *') !!}
                                {!! Form::email('email', '', ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                      {!! Form::label('subject', 'Subject*') !!}
                                {!! Form::text('subject', '', ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                 {!! Form::label('comment', 'Your comment...') !!}
                                {!! Form::textarea('comment', '', ['class' => 'form-control','rows' => 4,'required'=>'true']) !!}
                            </div>


                            <div class="col-md-12">
                                {!!Form::submit('SEND MESSAGE')!!}
                            </div>
                          {!! Form::close() !!} 
                    
                        <div class="clearfix"></div>				   

                    </div>	
                </div>

 @foreach($contact_content as $value)
                <div class="contact-info col-md-5">
                    <h4>Contact Info</h4>
                    <hr/>			

               
                
                {!!$value->content!!}
              
                	

                </div>

            </div>
  @endforeach
            <div class="clearfix"></div>	
        </div>	
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
@stop

@section('specific_js')
<!-- components-main.js -->
<script type="text/javascript"  src="{{url('/')}}/website_assets/js/customs/components-main.js"></script> 
@stop



