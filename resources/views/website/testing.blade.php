<!-- Meet The Team Begin -->
<div class="row section-wrapper bg-light-gray">
    <div class="container home-meet-team-2">

        <div class="col-md-12">
            <h2 class="text-dark header-big header-title-green-center margin-top-0 wow fadeInUp">CORPORATE TEAM</h2>


        </div>

        <div class="col-md-3 col-sm-6 wow fadeInUp">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>PAULO JOHN</h4>
                <p class="hmt-position">CEO/Founder</p>        

            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInLeft">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>MINE NAME</h4>
                <p class="hmt-position">CEO/Founder, Business development manager</p>
                
            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInRight">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>YOURS NAME</h4>
                <p class="hmt-position">Manager</p>
               
            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInUp">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>OTHERS NAME</h4>
                <p class="hmt-position">Client Manager</p>
            
            </div>
        </div>

    </div>
</div>
<!-- Meet The Team End -->