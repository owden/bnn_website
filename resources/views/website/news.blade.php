@extends('layouts.master')
@section('content')
<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">NEWS AND EVENTS</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->
<div class="row">
    <div class="container">
        <!--POST 1 BEGIN       
        ===============================================================--> 
        @foreach($news_content as $value)
        <div class="col-sm-6 col-md-6">

            <div class="thumbnail text-center">

                <!--sinister-hover-effects-->
                <div class="ImageWrapper BackgroundS">
                    <img src="{{ $value->gallery->file->path }}/{{ $value->gallery->file->name }}" alt="..." style="width:100%"/>
                    <div class="ImageOverlayH"></div>
                    <div class="StyleLi">
                        <span class="WhiteRounded">
                            <a class="test-popup-link" href="{{ $value->gallery->file->path }}/{{ $value->gallery->file->name }}">
                                <i class="flaticon-search19"></i>
                            </a>
                        </span>
                        <span class="WhiteRounded">
                            <a href="#"><i class="flaticon-link15"></i></a>
                        </span>
                    </div>
                </div>
                <!--sinister-hover-effects--> 

                <div class="caption">


                    <h4 class="plist-header-bold text-uppercase"><a href="#">{{ $value->title}}</a></h4>

                    {!!str_limit($value->content,200)!!}

                    <div class="bottom-line"></div>


                    <p class="text-center">
                        <a href="{!!url('/read_single_news/')!!}/{{ $value->webcontent_id }}">
                            <span class="btn1 btn1-default">CONTINUE READING</span>
                        </a>
                    </p>

                </div>
            </div>					
        </div>
        @endforeach
        <!--POST 1 END       
        ===============================================================-->    

        <div class="clearfix"></div> 

				<!-- PAGINATION
				===============================================================--> 
				<div class="col-md-12">
					<div class="pagination-buttons">						
                                           
						<ul>                        
						
                                                    {!!$news_content->links();!!}
                                            
						</ul>	
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- PAGINATION END
				===============================================================--> 

        <!-- Related Posts Begin					
        ===============================================================-->
        <div class="col-md-12">
            <h3 class="header-title">Related Posts</h3>

            <div id="owl-carousel">
                @foreach($news_content as $news)

                <!-- Carousel Item 1 Begin -->
                <div class="item">  
                    <div class="owl-thumb">
                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="{{ $news->gallery->file->path }}/{{ $news->gallery->file->name }}" alt="Owl Image"/>
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">
                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="{{ $news->gallery->file->path }}/{{ $news->gallery->file->name }}">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <h4 class="plist-header-bold"><p><a href="{!!url('/read_single_news/')!!}/{{ $news->webcontent_id }}">{{$news->title}}</a></p></h4>
                    </div>  
                </div>
                @endforeach
            </div><!-- owl-carousel-->	
        </div>
        <!-- Related Posts End -->					

    </div><!--container-->
</div>

@stop

@section('specific_js')                
<!-- Sticky Sidebar -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/theia-sticky-sidebar-master/theia-sticky-sidebar.js"></script>
<script type="text/javascript" src="{{url('/')}}/website_assets/js/theia-sticky-sidebar-master/test.js"></script>	
<!-- blog.js -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/customs/blog.js"></script>
@stop