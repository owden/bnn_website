@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">ABOUT US</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->

<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30" >
    <div class="container">
     
@foreach($about_content as $value)
   <div class="col-md-12 contact wow fadeInUp">      
         {!!$value->content !!}
        </div>
 @endforeach 
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
@stop

@section('specific_js')
<!-- components-main.js -->
<script type="text/javascript"  src="{{url('/')}}/website_assets/js/customs/components-main.js"></script> 
@stop



