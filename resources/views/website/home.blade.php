@extends('layouts.master')
@section('content')
        <!-- SLIDER BEGIN 
        ===============================================================-->
<div class="container">
    <div class="col-lg-12">
        <div class="example">
            <div class="content">

                <div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
                     data-alias="news-gallery34"
                     style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">

                    <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                    <div id="rev_slider_34_1" class="rev_slider fullwidthabanner" style="display:none;"
                         data-version="5.0.7">
                        <ul>    <!-- SLIDE  -->
                              @foreach($slide_content as $slide) 
                            <li data-index="rs-129" data-transition="parallaxvertical" data-slotamount="default"
                                data-easein="default" data-easeout="default" data-masterspeed="default"
                                data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                data-saveperformance="off" data-title="A JOY ON SWIMMING"
                                data-description="Generally We offer You a good Swimming Environment">
                                <!-- MAIN IMAGE -->
                                <img src="{{ $slide->gallery->file->path }}/{{ $slide->gallery->file->name }}" alt=""
                                     data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     data-bgparallax="10" class="rev-slidebg" data-no-retina/>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme rs-parallaxlevel-0"

                                     id="slide-129-layer-3"
                                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="full"
                                     data-height="full"
                                     data-whitespace="normal"
                                     data-transform_idle="o:1;"
                                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                     data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                     data-start="1000"
                                     data-basealign="slide"
                                     data-responsive_offset="on"

                                     style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);">

                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption Newspaper-Title   tp-resizeme rs-parallaxlevel-0"

                                     id="slide-129-layer-1"
                                     data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']"
                                     data-y="['top','top','top','top']" data-voffset="['165','135','105','130']"
                                     data-fontsize="['50','50','50','30']"
                                     data-lineheight="['55','55','55','35']"
                                     data-width="['600','600','600','420']"
                                     data-height="none"
                                     data-whitespace="normal"
                                     data-transform_idle="o:1;"
                                     data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                     data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"

                                     style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">
                                   {{ $slide->title }}

                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption Newspaper-Subtitle   tp-resizeme rs-parallaxlevel-0"

                                     id="slide-129-layer-2"
                                     data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']"
                                     data-y="['top','top','top','top']" data-voffset="['140','110','80','100']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;"
                                     data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                     data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"

                                     style="z-index: 7; white-space: nowrap;">BNN GROUP FOR YOU

                                </div>

                     
                            </li>
                             @endforeach()
                           
                        </ul>
                        <div class="tp-bannertimer tp-bottom"
                             style="height: 5px; background-color: rgba(166, 216, 236, 1.00);"></div>
                    </div>
                </div><!-- END REVOLUTION SLIDER -->
                <script type="text/javascript">
                    var tpj = jQuery;
                    var revapi34;
                    tpj(document).ready(function () {
                        if (tpj("#rev_slider_34_1").revolution == undefined) {
                            revslider_showDoubleJqueryError("#rev_slider_34_1");
                        } else {
                            revapi34 = tpj("#rev_slider_34_1").show().revolution({
                                sliderType: "standard",
                                jsFileLocation: "../../revolution/js/",
                                sliderLayout: "fullscreen",
                                dottedOverlay: "none",
                                delay: 9000,
                                navigation: {
                                    keyboardNavigation: "on",
                                    keyboard_direction: "horizontal",
                                    mouseScrollNavigation: "off",
                                    onHoverStop: "on",
                                    touch: {
                                        touchenabled: "on",
                                        swipe_threshold: 75,
                                        swipe_min_touches: 1,
                                        swipe_direction: "horizontal",
                                        drag_block_vertical: false
                                    }
                                    ,
                                    arrows: {
                                        style: "gyges",
                                        enable: true,
                                        hide_onmobile: false,
                                        hide_over: 778,
                                        hide_onleave: false,
                                        tmp: '',
                                        left: {
                                            h_align: "right",
                                            v_align: "bottom",
                                            h_offset: 40,
                                            v_offset: 0
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "bottom",
                                            h_offset: 0,
                                            v_offset: 0
                                        }
                                    }
                                    ,
                                    tabs: {
                                        style: "erinyen",
                                        enable: true,
                                        width: 250,
                                        height: 100,
                                        min_width: 250,
                                        wrapper_padding: 0,
                                        wrapper_color: "transparent",
                                        wrapper_opacity: "0",
                                        visibleAmount: 3,
                                        hide_onmobile: true,
                                        hide_under: 778,
                                        hide_onleave: false,
                                        hide_delay: 200,
                                        direction: "vertical",
                                        span: false,
                                        position: "inner",
                                        space: 10,
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 30,
                                        v_offset: 0
                                    }
                                },
                                viewPort: {
                                    enable: true,
                                    outof: "pause",
                                    visible_area: "80%"
                                },
                                responsiveLevels: [1240, 1024, 778, 480],
                                gridwidth: [1240, 1024, 778, 480],
                                gridheight: [500, 450, 400, 350],
                                lazyType: "none",
                                parallax: {
                                    type: "scroll",
                                    origo: "enterpoint",
                                    speed: 400,
                                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                                },
                                shadow: 0,
                                spinner: "off",
                                stopLoop: "off",
                                stopAfterLoops: -1,
                                stopAtSlide: -1,
                                shuffle: "off",
                                autoHeight: "off",
                                hideThumbsOnMobile: "off",
                                hideSliderAtLimit: 0,
                                hideCaptionAtLimit: 0,
                                hideAllCaptionAtLilmit: 0,
                                debugMode: false,
                                fallbacks: {
                                    simplifyAll: "off",
                                    nextSlideOnWindowFocus: "off",
                                    disableFocusListener: false,
                                }
                            });
                        }
                    });
                    /*ready*/
                </script>
            </div>
        </div>
    </div>
</div>
<!-- SLIDER END
===============================================================-->

<!-- HOME-1 BEGIN
===============================================================-->
<!-- Features-1 Begin -->
<div class="row section-wrapper-b30 bg-white">
    <div class="container">
        <div class="col-md-12">
<h2 class="text-dark header-big header-title-green-center margin-top-0 wow fadeInUp"> CORPORATE SERVICES </h2>

        </div>

        <div class="col-sm-4 home-services-2 wow bounceInLeft">
            <h3 class="header-xsmall">FINANCIAL SERVICES </h3>
            <p class="bottom-title margin-bottom-10">
               We have all financial facilities like bank ,mobile money and other kinds
            </p>        
        </div>

        <div class="col-sm-4 home-services-2 wow bounceInLeft">
            <h3 class="header-xsmall">INSURANCE</h3>
            <p class="bottom-title margin-bottom-10">
               Insurance facilities are available for you all the time
            </p>   
        </div>
        <div class="col-sm-4 home-services-2 wow bounceInLeft">
            <h3 class="header-xsmall">HOSPITALITY</h3>
            <p class="bottom-title margin-bottom-10">
                Hospitality is our way of operation ,Good management resulting into peacefully operation
            </p>
        </div>

        <div class="col-sm-4 home-services-2 wow bounceInLeft">
            <h3 class="header-xsmall">SHOPPING</h3>
            <p class="bottom-title margin-bottom-10">
                Get all shopping facilities at one premise
            </p>       
        </div>

    </div>
</div>
<!-- Features-1 End -->

<div class="clearfix"></div>

<!-- Parallax Services Begin -->
<div class="row">
    <section class="module parallax" style="background-image: url('{{url('')}}/website_assets/images/services/bnn-5.jpg')">

        <div class="bg-overlay">
            <div class="container">
                <div class="col-md-12">
                    <h2 class="text-black header-big header-title-green-center margin-top-0 wow fadeInUp">OUR
                        SERVICES </h2>

                </div>
                <div class="col-md-4 col-sm-6 wow zoomIn">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-coffee text-green"></i>
                        </div>
                        <h4>FOOD & BEVERAGES </h4>
                        <p>

                            Come enjoy food and bevarages.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-4 col-sm-6 wow zoomIn">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-ios-nutrition"></i>
                        </div>
                        <h4>ACCOMMODATION </h4>
                        <p>
                           Good accomodation for every one.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-android-open"></i>
                        </div>
                        <h4>MOBILE MONEY</h4>
                        <p>
                            Good mobile money for every one         </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-arrow-graph-up-right"></i>
                        </div>
                        <h4>BUREAU DE CHANGE</h4>
                        <p>
                           No hassle exchange rate for you.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.6s">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-earth"></i>
                        </div>
                        <h4>HALOTEL SUPER DEALER</h4>
                        <p>
                            Good accomodation for you.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.8s">
                    <div class="home-services-item">
                        <div class="home-services-icon">
                            <i class="icon ion-headphone"></i>
                        </div>
                        <h4>FASHIONS & BOUTIQUE</h4>
                        <p>
                           get botique you like
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
                
        </div>
    </section>
</div>
<!-- Parallax Services End -->

<div class="clearfix"></div>

<!-- Meet The Team Begin -->
<div class="row section-wrapper bg-light-gray">
    <div class="container home-meet-team-2">

        <div class="col-md-12">
            <h2 class="text-dark header-big header-title-green-center margin-top-0 wow fadeInUp">CORPORATE TEAM</h2>


        </div>

        <div class="col-md-3 col-sm-6 wow fadeInUp">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>PAULO JOHN</h4>
                <p class="hmt-position">CEO/Founder</p>        

            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInLeft">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>MINE NAME</h4>
                <p class="hmt-position">CEO/Founder, Business development manager</p>
                
            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInRight">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>YOURS NAME</h4>
                <p class="hmt-position">Manager</p>
               
            </div>
        </div>

        <div class="col-md-3 col-sm-6 wow fadeInUp">
            <div class="hmt2-item">
                <a href="#"><img src="{{url('/')}}/website_assets/images/about_us/team/avatar.png" alt=""/></a>
                <h4>OTHERS NAME</h4>
                <p class="hmt-position">Client Manager</p>
            
            </div>
        </div>

    </div>
</div>
<!-- Meet The Team End -->

<div class="clearfix"></div>

<!-- Our Clients Begin -->
<div class="row section-wrapper bg-white border-tb">
    <div class="container">

        <div class="col-md-12 au-clients">
            <h2 class="text-dark header-big header-title-green-center margin-top-0 wow fadeInUp">GROUP
                AFFILLIATES</h2>

            <div id="owl-carousel-clients">

                <!-- Item 1 Begin-->
                <div class="item">
                    <a href="#">
                        
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/mwanza.png" alt="Mwanza"/>
                    </a>
                </div>
                <!-- Item 1 End-->

                <!-- Item 2 Begin-->
                <div class="item">
                    <a href="#">
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/mtwara.png" alt=""/>
                    </a>
                </div>
                <!-- Item 2 End-->

                <!-- Item 3 Begin-->
                <div class="item">
                    <a href="#">
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/dsm.png" alt=""/>
                    </a>
                </div>
                <!-- Item 3 End-->

                <!-- Item 4 Begin-->
                <div class="item">
                    <a href="#">
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/mwanza.png" alt=""/>
                    </a>
                </div>
                <!-- Item 4 End-->

                <!-- Item 5 Begin-->
                <div class="item">
                    <a href="#">
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/mtwara.png" alt=""/>
                    </a>
                </div>
                <!-- Item 5 End-->

         <div class="item">
                    <a href="#"> 
                        <img src="{{url('/')}}/website_assets/images/about_us/clients/dsm.png" alt="DSM"/>
                    </a>
                </div>
                <!-- Item 1 End-->
            </div>

        </div>
    </div>
</div>
<!-- Our Clients End -->

<!-- HOME-1 END
===============================================================-->
@stop

@section('specific_js')
        <!-- Counterup.min.js -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="{{url('/')}}/website_assets/js/jquery.counterup.min.js"></script>
<!-- index.js -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/customs/index.js"></script>
@stop
