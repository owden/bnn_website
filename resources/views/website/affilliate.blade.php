
@extends('layouts.master')
@section('content')	
<div class="clearfix"></div>

<!-- Page Header End -->
<div class="row">
    <div class="container">

        <!-- LEFT SIDE BEGIN           
        ===============================================================-->	
        <div class="col-md-8">

            <!-- SINGLE POST BEGIN           
            ===============================================================-->	
            <div class="row">

                <div class="col-sm-12 col-md-12">  

                    <div class="thumbnail text-left">


                        <div id="owl-slider-post" class="owl-carousel owl-theme">
                          
                       @foreach($content as $value)
                         
               <div class="item">
                                <!--sinister-hover-effects-->
                                <div class="ImageWrapper">
                                    <img src="{{$value->gallery->file->path}}/{{$value->gallery->file->name}}" alt=""/>
                                    <div class="ImageOverlayH"></div>
                                    <div class="StyleLi">
                                        <span class="WhiteRounded">
                                            <a class="gallery-item" href="{{$value->gallery->file->path}}/{{$value->gallery->file->name}}">
                                                <i class="flaticon-search19"></i>
                                            </a>
                                        </span>                                 
                                    </div>
                                </div>
                                <!--sinister-hover-effects-->    
                            </div>               
                             
                   @endforeach
                            
                        
                       
                        </div>    


                        <div class="caption" style="margin-top:10px">
                         
@foreach($content as $value)

{!! $value->content !!}

 @endforeach
                        </div>
                    </div>

                </div>  

                <div class="clearfix"></div> 

            </div> <!--row-->  



            <!-- ABOUT THE AUTHOR END           
            ===============================================================-->	

            <!-- RELATED POSTS BEGIN					
            ===============================================================-->
            <h3 class="header-title-center">Related Posts</h3>

            <div id="owl-carousel">
 @foreach($content as $value)
                <!-- Carousel Item 1 Begin -->
                <div class="item">  
                    <div class="owl-thumb">

                        <!--sinister hover effects begin-->
                        <div class="ImageWrapper BackgroundS">
                            <img src="{{$value->gallery->file->path}}/{{$value->gallery->file->name}}"/>  
                            <div class="ImageOverlayH"></div>
                            <div class="StyleLi">

                                <span class="WhiteRounded">
                                    <a class="test-popup-link" href="{{$value->gallery->file->path}}/{{$value->gallery->file->name}}">
                                        <i class="flaticon-search19"></i>
                                    </a>
                                </span>	

                                <span class="WhiteRounded">
                                    <a href="#"><i class="flaticon-link15"></i></a>
                                </span>

                            </div>
                        </div>
                        <!--sinister hover effects end-->

                        <div class="post-category text-center margin-top-10 margin-bottom-5">          
                         <h4 class="plist-header-bold"><a href="#">{{$value->title}}</a></h4>         
                        </div>

                       
                    </div>  
                </div>
 @endforeach
          		  			   
            </div><!-- owl-carousel-->
            <!-- RELATED POSTS END					
            ===============================================================-->




            <!-- COMMENTS END					
            ===============================================================-->

        </div>
        <!-- LEFT SIDE END           
        ===============================================================-->

        <!-- SIDEBAR BEGIN           
        ===============================================================-->
        <div class="col-md-4 Sidebar">		 
            <div class="theiaStickySidebar"> 				
                <div class="row"> 


                    <!-- INSTAGRAM BEGIN           
                    ===============================================================-->
                    <div class="col-sm-12 col-md-12">					  
                      <div class="thumbnail sb-newsletter">

                            <div class="widget-header-line"></div>				
                            <h3 class="sb-header text-center">NEWSLETTER</h3>					  
                            <div class="widget-header-line"></div>

                            <p>
                                Enter your email address to subscribe to this website and receive notifications of new posts by email.
                            </p>

                             {!! Form::open(['url' => 'subscribe', 'method' => 'POST','id'=>'subscribe']) !!}

                        {{csrf_field()}}
                                
                                <p> {{Form::hidden('id', $menu_id)}}</p>
                                 <p> {{Form::hidden('type', '0')}}</p>
                          
                                <p>
                                {!! Form::email('email', '', array('class' => 'subscribe-textbox','required'=>'true','placeholder'=>'Email Address')) !!}
</p>
                                <p> 
                                      @if(session('success'))
                    <div class="alert alert-success alert-dismissible">{{session('success')}}</div>
                    @elseif(session('error'))
                           <div class="alert alert-danger alert-dismissible">{{session('error')}}</div>
                           @endif
                           <p>
                            
                                         {!!Form::submit('SUBSCRIBE', array('class' => 'btn btn-primary btn-corner'))!!}
                        </p> 
                                
                             {!! Form::close() !!}
                        </div>

                    </div>               
                </div><!--row-->

            </div><!--Sticky Sidebar-->

        </div><!--md-4 (Sidebar End)-->		
        <!-- SIDEBAR END           
        ===============================================================-->	

    </div><!--container-->
</div>

<div class="clearfix"></div>

@stop



@section('specific_js')
<!-- Sticky Sidebar -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/theia-sticky-sidebar-master/theia-sticky-sidebar.js"></script>
<script type="text/javascript" src="{{url('/')}}/website_assets/js/theia-sticky-sidebar-master/test.js"></script>
<!-- blog.js -->
<script type="text/javascript" src="{{url('/')}}/website_assets/js/customs/blog.js"></script> 

@stop