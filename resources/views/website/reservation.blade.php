@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<!-- Page Header Begin -->
<div class="row" id="page-bar">
    <div class="page-header-one">
        <div class="container">
            <div class="col-lg-12">
                <h3 class="pull-left">RESERVATION</h3>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->


<!-- CONTACT BEGIN           
===============================================================-->
<div class="row margin-bottom-30">
    <div class="container" >
        <div class="col-md-12 contact wow fadeInUp">		
            <div class="row">
                <div class="contact-form col-md-7">
                    <h4>Reservation Form</h4>
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible">{{session('success')}}</div>
                    @elseif(session('error'))
                           <div class="alert alert-danger alert-dismissible">{{session('error')}}</div>
                           @endif
                    <hr/>
           <div class="contact-form-area2">
                        {!! Form::open(['url' => 'reservation', 'method' => 'POST','id'=>'reservation']) !!}

                        {{csrf_field()}}
                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('firstname', 'Firstname') !!}
                                {!! Form::text('firstname', '', ['class' => 'form-control','required'=>'true']) !!}

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">						  
                                {!! Form::label('lastname', 'Lastname') !!}
                                {!! Form::text('lastname', '', ['class' => 'form-control','required'=>'true']) !!} </div>
                        </div>

                        <div class="col-md-6">

                            <div  class="form-group">
                                {!! Form::label('checkin_date', 'Checkin date') !!}
                                {!! Form::date('checkin_date', '', ['class' => 'form-control','required'=>'true']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('checkout_date', 'Checkout date') !!}
                                {!! Form::date('checkout_date', '', ['class' => 'form-control','required'=>'true']) !!}
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('adult', 'Adult') !!}
                                {!! Form::number('adult', '', ['class' => 'form-control']) !!}
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('children', 'Children') !!}
                                {!! Form::number('children', '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', '', ['class' => 'form-control','required'=>'true']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  class="form-group">
                                {!! Form::label('phonenumber', 'Phonenumber') !!}
                                {!! Form::text('phonenumber', '', ['class' => 'form-control','required'=>'true']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div  class="form-group">
                                {!! Form::label('other', 'Other Requirements') !!}
                                {!! Form::textarea('other', '', ['class' => 'form-control','rows' => 2]) !!}
                            </div>
                        </div>


                        <div class="col-md-12">
                            {!!Form::submit('SUBMIT')!!}
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>				   

                    </div>	
                </div>


                <div class="contact-info col-md-5">
                    <h4>Reservation important Info</h4>
                    <hr/>			

                    <p>
                       BNN assures you to give you service tha you will enjoy and be glad
                    </p>



                </div>

            </div>

            <div class="clearfix" ></div>	
        </div>	
    </div>

</div>
<!-- CONTACT END           
===============================================================-->

<div class="clearfix"></div>
@stop

@section('specific_js')
<!-- components-main.js -->
<script type="text/javascript"  src="{{url('/')}}/website_assets/js/customs/components-main.js"></script> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<!--<script  src="{{url('/')}}/website_assets/assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>-->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@stop



