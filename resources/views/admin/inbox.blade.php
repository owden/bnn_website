<?php
?>
@extends('admin.layouts.master')
@section('content') 
       <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link href="<?=  asset_url()?>plugins/icheck/skins/minimal/minimal.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Mailbox</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="mail-inbox.html">Mailbox</a>
                        </li>
                        <li class="active">
                            <strong>Inbox</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    
<div class="col-lg-12">
    <section class="box nobox ">
                <div class="content-body">    <div class="row">

        <div class="col-md-3 col-sm-4 col-xs-12">
            

    <a class="btn btn-primary btn-block" href='mail-compose.html'>Compose</a>

    <ul class="list-unstyled mail_tabs">
        <li class="active">
            <a href="mail-inbox.html">
                <i class="fa fa-inbox"></i> Inbox <span class="badge badge-primary pull-right">6</span>
            </a>
        </li>
        <li class="">
            <a href="mail-sent.html">
                <i class="fa fa-send-o"></i> Sent
            </a>
        </li>
        <li class="">
            <a href="mail-drafts.html">
                <i class="fa fa-edit"></i> Drafts <span class="badge badge-accent pull-right">2</span>
            </a>
        </li>
        <li class="">
            <a href="mail-important.html">
                <i class="fa fa-star-o"></i> Important
            </a>
        </li>
        <li class="">
            <a href="mail-trash.html">
                <i class="fa fa-trash-o"></i> Trash
            </a>
        </li>
    </ul>

            </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="mail_content">

                <div class="row">
                    <div class="col-xs-12">
                        
    <h3 class="mail_head">Inbox <sup>(5)</sup></h3>
    <i class='fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-search icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right'></i>


                        </div>

                    <div class="col-xs-12">

                        <div class="pull-left">
                            <div class="btn-group mail_more_btn">
                                <button type="button" class="btn btn-default"> All</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">All</a></li>
                                    <li><a href="#">Read</a></li>
                                    <li><a href="#">Unread</a></li>
                                    <li><a href="#">Starred</a></li>
                                </ul>
                            </div>
                        </div>

                        <nav class='pull-right'>
                            <ul class="pager mail_nav">
                                <li><a href="#"><i class='fa fa-arrow-left icon-xs icon-accent icon-secondary'></i></a></li>
                                <li><a href="#"><i class='fa fa-arrow-right icon-xs icon-accent icon-secondary'></i></a></li>
                            </ul>
                        </nav>
                        <span class='pull-right mail_count_nav text-muted'><strong>1</strong> to <strong>20</strong> of 3247</span>

                    </div>

                    <div class="mail_list col-xs-12">
                        <table class="table table-striped table-hover">
                                                        <tr class="unread" id="mail_msg_1">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-primary">Family</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">19:34</span></td>
                            </tr>
                                                        <tr class="unread" id="mail_msg_2">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">21:54</span></td>
                            </tr>
                                                        <tr class="unread" id="mail_msg_3">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">22:28</span></td>
                            </tr>
                                                        <tr id="mail_msg_4">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">Yesterday</span></td>
                            </tr>
                                                        <tr id="mail_msg_5">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">Yesterday</span></td>
                            </tr>
                                                        <tr id="mail_msg_6">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="label label-info">Work</span>&nbsp;<span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">28 Feb</span></td>
                            </tr>
                                                        <tr class="unread">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-info">Work</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">25 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_8">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">25 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_9">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">25 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_10">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-success">IMP</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">25 Feb</span></td>
                            </tr>
                                                        <tr class="unread">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">21 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_12">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">21 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_13">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-success">IMP</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">21 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_14">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">21 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_15">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">21 Feb</span></td>
                            </tr>
                                                        <tr class="unread">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-info">Work</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">17 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_17">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">17 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_18">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">17 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_19">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-danger">Urgent</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">17 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_20">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">17 Feb</span></td>
                            </tr>
                                                        <tr class="unread">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">16 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_22">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">John Smith</td>
                                <td class="open-view"><span class="label label-primary">Family</span>&nbsp;<span class="msgtext">Hello, hope you having a great day ahead.</span></td>
                                <td class="open-view"><span class="msg_time">16 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_23">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Laura Willson</td>
                                <td class="open-view"><span class="msgtext">Your product seems amazingly smart and elegant to use.</span></td>
                                <td class="open-view"><span class="msg_time">16 Feb</span></td>
                            </tr>
                                                        <tr id="mail_msg_24">
                                <td class=""><input class="iCheck" type="checkbox"></td>
                                <td class=""><div class="star"><i class='fa fa-star-o icon-xs icon-accent'></i></div></td>
                                <td class="open-view">Jane D.</td>
                                <td class="open-view"><span class="msgtext">We play, dance and love. Share love all around you.</span></td>
                                <td class="open-view"><span class="msg_time">16 Feb</span></td>
                            </tr>
                                                    </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
    </div>
        </section></div>


    </section>
    </section>
    <!-- END CONTENT -->
    @stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@stop