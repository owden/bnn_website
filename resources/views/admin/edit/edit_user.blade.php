<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


@extends('admin.layouts.master')

@section('content')
     <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link  href="<?=  asset_url()?>plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>       
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Edit User</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>Edit User</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
<div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">User Account Info</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">
    <div class="row">
   <form action="#" method="post">
                    <div class="col-xs-12 col-sm-9 col-md-8">
                        
			<div class="form-group">
                            <label class="form-label" >Email</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="text" value="email@example.com" class="form-control" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Phone</label>
                            <span class="desc">e.g. "(534) 253-5353"</span>
                            <div class="controls">
                                <input type="text" value="(123) 456-7878" class="form-control"  data-mask="phone"  placeholder="(999) 999-9999">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Password</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="password" value="abcdefgh" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Confirm Password</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="password"  value="abcdefgh" class="form-control" id="field-21">
                            </div>
                        </div>

                    </div>
 

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                            	<div class="text-left">
                                <button type="button" class="btn btn-primary">Save</button>
                                <button type="button" class="btn">Cancel</button>
                            </div>
                        </div>
                    </form>
    </div>


    </div>
        </section></div>

    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datepicker/js/datepicker.js" type="text/javascript"></script> 

@stop