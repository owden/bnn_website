<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')
@section('content')
     <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link  href="<?=  asset_url()?>plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>       
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->  

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">CONTACT US</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                       
                        <li class="active">
                            <strong>Edit Contact details</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Basic Info</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">
                  @foreach($update_contact as $value)
    <div class="row">
    <form action ="#" method="post" id="form">


                    <div class="col-xs-12">
                    
    <div class="form-group">
      
                            <label class="form-label" >Contact Content</label>
                            <span class="desc"></span>

                            <textarea class="ckeditor"cols="80" id="editor1" name="editor1" rows="10">
             {!!$value->content !!} </textarea><br>
                    </div>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                            <input type="text" value="{{$value->webcontent_id}}" id="webcontent_id" name="webcontent_id" hidden="true"/>
                        </div>

  <div class="col-xs-12" id="result"></div>
                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                             	<div class="text-left">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                   
    </div>
 </form>
    </div>
  @endforeach
    </div>
        </section></div>



    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datepicker/js/datepicker.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/ckeditor/ckeditor.js" type="text/javascript">  
</script>

<script type="text/javascript">

	$(document).ready(function() {
               var siteURL="{{url('/update_contact')}}";  
		$('#form').bind('submit', function(e) {
			e.preventDefault(); // <-- important
			$(this).ajaxSubmit({
			
  url: siteURL,
  method: "POST",
  data: $(e).serialize(),
  beforeSend: function() {
      return true;
    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  },
  success:function( data ) {
  infoAlert('Info', data.message);
    }
  });
  });
  });	
        </script>
@stop