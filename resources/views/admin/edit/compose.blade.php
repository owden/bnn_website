<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('admin.layouts.master')
@section('content') 
      <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link href="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?=  asset_url()?>plugins/tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" media="screen"/>   
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Mailbox</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="mail-inbox.html">Mailbox</a>
                        </li>
                        <li class="active">
                            <strong>Compose</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    

<div class="col-lg-12">
    <section class="box nobox ">
                <div class="content-body">    <div class="row">

        <div class="col-md-3 col-sm-4 col-xs-12">
            

    <a class="btn btn-primary btn-block" href='mail-compose.html'>Compose</a>

    <ul class="list-unstyled mail_tabs">
        <li class="">
            <a href="mail-inbox.html">
                <i class="fa fa-inbox"></i> Inbox <span class="badge badge-primary pull-right">6</span>
            </a>
        </li>
        <li class="">
            <a href="mail-sent.html">
                <i class="fa fa-send-o"></i> Sent
            </a>
        </li>
        <li class="">
            <a href="mail-drafts.html">
                <i class="fa fa-edit"></i> Drafts <span class="badge badge-accent pull-right">2</span>
            </a>
        </li>
        <li class="">
            <a href="mail-important.html">
                <i class="fa fa-star-o"></i> Important
            </a>
        </li>
        <li class="">
            <a href="mail-trash.html">
                <i class="fa fa-trash-o"></i> Trash
            </a>
        </li>
    </ul>

            </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="mail_content">

                <div class="row">
                    <div class="col-xs-12">
                        
    <h3 class="mail_head">Compose</h3>
    <i class='fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-search icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right'></i>


                        </div>

                    <div class="col-xs-12 mail_view_title">

                        <div class='pull-right'>
                            <button type="button" class="btn btn-default btn-icon"  data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Send" data-placement="top">
                                <i class="fa fa-paper-plane-o icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon"  data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Save" data-placement="top">
                                <i class="fa fa-floppy-o icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon"  data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Trash" data-placement="top">
                                <i class="fa fa-trash-o icon-xs"></i>
                            </button>
                        </div>

                    </div>

                    <div class="col-xs-12 mail_view_info">

                        <div class="form-group mail_cc_bcc">
                            <label class="form-label">To:</label>
                            <span class="desc">e.g. "someemail@example.com"</span>
                            <div class="labels"><span class='label label-secondary cc'>CC</span> <span class='label label-secondary bcc'>BCC</span>
                            </div>
                            <div class="controls">
                                <input type="text" class="form-control mail_compose_to" value="" />
                            </div>
                        </div>

                        <div class="form-group mail_compose_cc">
                            <label class="form-label">CC:</label>
                            <span class="desc">e.g. "someemail@example.com"</span>
                            <div class="controls">
                                <input type="text" class="form-control mail_compose_to" value="" />
                            </div>
                        </div>

                        <div class="form-group mail_compose_bcc">
                            <label class="form-label">BCC:</label>
                            <span class="desc">e.g. "someemail@example.com"</span>
                            <div class="controls">
                                <input type="text" class="form-control mail_compose_to" value="" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Subject:</label>
                            <span class="desc">e.g. "Meeting in 1st week"</span>
                            <div class="controls">
                                <input type="text" class="form-control" value="" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Message:</label>
                            <textarea class="mail-compose-editor" placeholder="Enter text ..." style="width: 100%; height: 250px; font-size: 14px; line-height: 23px;padding:15px;"></textarea>
                        </div>



                    </div>





                    <div class="col-xs-12">

                        <div class='pull-left'>
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-paper-plane-o icon-xs"></i> &nbsp; SEND
                            </button>
                            <button type="button" class="btn btn-purple">
                                <i class="fa fa-floppy-o icon-xs"></i> &nbsp; SAVE
                            </button>
                            <button type="button" class="btn btn-secondary">
                                <i class="fa fa-trash-o icon-xs"></i> &nbsp; TRASH
                            </button>
                        </div>

                    </div>





                </div>

            </div>
        </div>

    </div>
    </div>
        </section></div>


    </section>
    </section>
    <!-- END CONTENT -->
        @stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/tagsinput/js/bootstrap-tagsinput.min.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/js/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script><
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


@stop