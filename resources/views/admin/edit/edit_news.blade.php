<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')
@section('content')
     <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link  href="{{url('/')}}/admin_assets/assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>       
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->  

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Edit News/Events</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Edit News/Event</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Basic Info</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">
                  @foreach($edit_single_news as $value)
    <div class="row">
    <form action ="#" method="post" id='form'>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                                    <div class="form-group">
                            <label class="form-label" for="field-17215">Title</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="text" value="{{$value->title}}" class="form-control" id="title" name="title">
                            </div>
                        </div>

                        </div>

                    <div class="col-xs-12">
                    
    <div class="form-group">
      
                            <label class="form-label" >News/Event Content</label>
                            <span class="desc"></span>

                            <textarea class="ckeditor" cols="80" id="content" name="content" rows="10">
             {!!$value->content !!} 
                            </textarea><br>
                    </div>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                       
                            <input type="text" value="{{$value->webcontent_id}}" id='webcontent_id' name='webcontent_id' hidden="true">
                        <div class="form-group">
                            <label class="form-label" >Upload Image</label>
                            <span class="desc"></span>
                                                        <img class="img-responsive" src="{{ $value->path }}/{{ $value->name }}" alt="" style="max-width:120px;">
                                                        <div class="controls">
                                <input type="file" class="form-control" id="pic" name='pic'>
                            </div>
                        </div>
                       
        


                        </div>

     
                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                             <span class="desc" id="loader" hidden="true">   <?=loader()?></span>
                             	<div class="text-left">
                                  
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                   
    </div>
 </form>
    </div>
  @endforeach
    </div>
        </section></div>



    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="{{url('/')}}/admin_assets/assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script> 
<script src="{{url('/')}}/admin_assets/assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/ckeditor/ckeditor.js" type="text/javascript">  
</script>

<script type="text/javascript">

	$(document).ready(function() {
               var siteURL="{{url('/')}}/update_news";  
		$('#form').bind('submit', function(e) {
			e.preventDefault(); // <-- important
                        CKupdate();
			$(this).ajaxSubmit({
			
  url: siteURL,
  method: "POST",
  enctype:"multipart/form-data",
  data: $(e).serialize(),
  beforeSend: function() {
      $('#loader').show();
      return true;
    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  },
  success:function( data ) {
        $('#loader').hide();
  infoAlert('Info', data.message);
    }
  });
  });
  });
     CKupdate=function(){
      for(var instanceName in CKEDITOR.instances)
    CKEDITOR.instances[instanceName].updateElement();
  };
        </script>
@stop