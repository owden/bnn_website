<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


@extends('admin.layouts.master')

@section('content')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link href="<?=  asset_url()?>plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?=  asset_url()?>plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?=  asset_url()?>plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?=  asset_url()?>plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>    
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link  href="<?=  asset_url()?>plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>       
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Add a User</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Visit Website</a>
                        </li>
                     
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
<div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">User Account Info</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">
    <div class="row">
    <form id='form'>
                    <div class="col-xs-12 col-sm-9 col-md-8">
                    <div class="form-group">
                            <label class="form-label" >Full Name</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="text" value="" class="form-control" name='name' id='name' required="true">
                            </div>
                        </div>
			<div class="form-group">
                            <label class="form-label" >Email</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="email" value="" class="form-control" name='email' id='email' required="true">
                            </div>
                        </div>
<!--                    
                        <div class="form-group">
                            <label class="form-label" >Phone</label>
                            <span class="desc">e.g. "(534) 253-5353"</span>
                            <div class="controls">
                                <input type="text" value="" class="form-control" name='phonenumber' data-mask="phone"  placeholder="(999) 999-9999" id='phonenumber' required="true">
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="form-label" >Password</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="password" value="" class="form-control"name='password'id='password' required="true" minlength="6" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Confirm Password</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="password"  value="" class="form-control" id="password_confirmation" name='password_confirmation' id='confirm_password'>
                            </div>
                        </div>
                         <div class="form-group">
                          <div id="result" ></div>   
                         </div>
                    </div>
 
      
                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                             <span class="desc" id="loader" hidden="true">   <?=loader()?></span>
                           	<div class="text-left">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
    </div>


    </div>
        </section></div>


    </section>
    </section>
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datepicker/js/datepicker.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->
<script type="text/javascript">

	$(document).ready(function() {
               var siteURL="{{url('/')}}/add_user";  
		$('#form').bind('submit', function(e) {
			e.preventDefault(); // <-- important
			$(this).ajaxSubmit({
			
  url: siteURL,
  method: "POST",
  data: $(e).serialize(),
  beforeSend: function() {
        $('#loader').show();
   // infoAlert('info', $('#form').serialize());
      return true;
    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  },
  success:function( data ) {
        $('#loader').hide();
 infoAlert(data.message);
 
    }
  });
  });
  });	
        </script>
@stop
