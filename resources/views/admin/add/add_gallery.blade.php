<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')

@section('content')
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link href="<?=  asset_url()?>plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?=  asset_url()?>plugins/select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>        

        <link href="<?=  asset_url()?>plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">ADD TO GALLERY</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                       
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    
<div class="col-lg-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Gallery upload</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
        <div class="col-xs-12">

                <div class="col-xs-8">
                    <form id="mydropzone" class="dropzone">
                             <div class="form-group">
                <label class="form-label">Choose category</label>
                <select class="" id="s2example-1" name="webpart_id">
                    <option></option>
                        
                        <?php
                        $sub_list=  App\Models\Webpart::whereNotIn('webpart_id',array(2,3,4,6,7,8,9,10))->get();
                       // dd($sub_list);
                        foreach ($sub_list as $sub){?>
                    
                           <option value="{{$sub->webpart_id}}">{{$sub->name}}</option>   
                       <?php }?>
                    
                </select>

 
            </div>
                <div class="form-group">
                    <input type="text"  maxlength="70" class="form-control" name="title" id="title" placeholder="Enter image description..">
                    
                </div>
                        
                        <div class="form-group" id="dropzonePreview"></div>
                        <div class="form-group">
                           
                            <span class="btn btn-success fileinput-button">
		<i class="glyphicon glyphicon-plus"></i>
		<span>Add file...</span>
	    </span>
                        </div>
                        <div class="form-group">
                             {!! csrf_field() !!}
                        </div>

            </form> 
                <div class="form-group">
                    <button type="button" id="submit" class="btn btn-primary">Upload</button>
              </div>
                </div>
        
                



        </div>
    </div>
    </div>
        </section></div>


    </section>
    </section>
    <!-- END CONTENT -->
    @stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/select2/select2.min.js" type="text/javascript"></script> 

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<script type="text/javascript">
     var siteURL="{{url('/')}}"; 
                 Dropzone.options.mydropzone = {
// url does not has to be written if we have wrote action in the form tag but i have mentioned here just for convenience sake 
          url: siteURL+"/add_gallery", 
          addRemoveLinks: true,
          autoProcessQueue: false, // this is important as you dont want form to be submitted unless you have clicked the submit button
          autoDiscover: false,
          paramName: 'pic', // this is optional Like this one will get accessed in php by writing $_FILE['pic'] // if you dont specify it then bydefault it taked 'file' as paramName eg: $_FILE['file'] 
          previewsContainer: '#dropzonePreview', // we specify on which div id we must show the files
          clickable:".fileinput-button", // this tells that the dropzone will not be clickable . we have to do it because v dont want the whole form to be clickable 
          accept: function(file, done) {
            console.log("uploaded");
            done();
          },
          success:function (file,data){
          infoAlert('Info', data.message);
              
          },
         error: function(file, data){
            errorAlert('Error!', data.message);;
          },
          init: function() {

              var myDropzone = this;
            //now we will submit the form when the button is clicked
            $("#submit").on('click',function(e) {
               e.preventDefault();
               myDropzone.processQueue(); // this will submit your form to the specified action path
              // after this, your whole form will get submitted with all the inputs + your files and the php code will remain as usual 
        //REMEMBER you DON'T have to call ajax or anything by yourself, dropzone will take care of that
            });

          } // init end

        };
	
        </script>
@stop
