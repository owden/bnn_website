<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

@extends('admin.layouts.master')

@section('content')
   <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link href="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Editing About Us Form</h1>  </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    
<div class="col-lg-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left"></h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body"><div class="row">
        <div class="col-xs-12">

            <textarea class="bootstrap-wysihtml5-textarea" id="content" name="content" placeholder="Enter text ..." style="width: 100%; height: 250px; font-size: 14px; line-height: 23px;padding:15px;" required="true"></textarea>

        </div>
    </div>
    </div>
        </section></div>
    <div id="result" class="col-lg-12"></div>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                             	<div class="text-left">
                                    <button type="submit" class="btn btn-primary" onmousedown="add_contact()">Submit</button>
                            </div>
                        </div>
    </section>
    </section>
@stop

@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/js/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<script type="text/javascript">
         var siteURL="{{url('/')}}/add_about";    
           function add_contact() {
         //NProgress.start();
            var content=$('#content').val();
            if(content===''){
          $('#result').html('<div class="alert alert-error alert-dismissible fade in">Please write the content first</div>');           
           return false;
       }
            $.post(siteURL, {content: content}, function (data) {
              
                infoAlert('Info', 'successfully Submitted.');
        },'json');
            }
    
</script>
@stop