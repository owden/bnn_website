<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')

@section('content')
     <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link  href="<?=  asset_url()?>plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>       
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->  
    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Add News</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>Add News</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Basic Info</h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">
    <div class="row">
    <form action ="#" method="post" id="form">

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                                    <div class="form-group">
                            <label class="form-label" for="field-1676"> Title</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <input type="text" value="" class="form-control" id="title" name="title" required="true">
                            </div>
                       
                           
                        </div>
                            <div class="form-group">
                  <input type="text" value="3"  id="webpart_id" name="webpart_id" hidden="true">
                         
                            </div>

                        </div>

                    <div class="col-xs-12">
                    
    <div class="form-group">
                            <label class="form-label" >News Content</label>
                            <span class="desc"></span>

                            <textarea required="true" class="ckeditor" cols="80" id="content" name="content" rows="10">
            </textarea><br>
                    </div>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                        
                  

                        <div class="form-group">
                            <label class="form-label" >Upload your Image</label>
                            <span class="desc"></span>
                                                        <div class="controls">
                                                            <input type="file" class="form-control" name="pic"  required="true" >
                            </div>
                        </div>
                
                  
          
                        </div>
                        <div class="col-xs-12" id="result"></div>

                        <div class="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                              <span class="desc" id="loader" hidden="true">   <?=loader()?></span>
                             	<div class="text-left">
                                <button type="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                   
    </div>
 </form>
    </div>
    </div>
        </section></div>



    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datepicker/js/datepicker.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/ckeditor/ckeditor.js" type="text/javascript">   </script>

<script type="text/javascript">

	$(document).ready(function() {
               var siteURL="{{url('/')}}/add_news";  
		$('#form').bind('submit', function(e) {
			e.preventDefault(); // <-- important
                        CKupdate();
			$(this).ajaxSubmit({
			
  url: siteURL,
  method: "POST",
  data: $(e).serialize(),
  beforeSend: function() {
        $('#loader').show();
     // alert( $('#form').serialize());
      return true;
    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  },
  success:function( data ) {
        $('#loader').hide();
 infoAlert('Info', data.message);
  
    }
  });
  });
  });
    CKupdate=function(){
      for(var instanceName in CKEDITOR.instances)
    CKEDITOR.instances[instanceName].updateElement();
  };
        </script>
@stop