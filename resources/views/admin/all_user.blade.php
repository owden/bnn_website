<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


@extends('admin.layouts.master')

@section('content')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link href="<?=  asset_url()?>plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="screen"/><link href="assets/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css" rel="stylesheet" type="text/css" media="screen"/><link href="assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet" type="text/css" media="screen"/><link href="assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>    
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

        <div class='col-xs-12'>
            <div class="page-title">

                <div class="pull-left">
                    <h1 class="title">Users</h1>                            </div>

                <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!!url('/')!!}"><i class="fa fa-home"></i>Visit Website</a>
                        </li>
                       
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">All Users</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                        <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                        <a class="box_close fa fa-times"></a>
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                        <div class="col-xs-12">
                            <!-- ********************************************** -->


                            <table id="example" class="display table table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Name</th><th>Email</th><th>Phone</th><th>Added On</th>                 
                                    </tr>
                                </thead>

                                <tbody>
                                          @foreach($all_user as $value)
                                          <tr><td>{{ $value->id }}</td>
                                              <td>{{ $value->name }}</td>
                                              <td>{{ $value->email }}</td>
                                              <td>{{ $value->phonenumber }}</td>
                                              <td>{{ $value->created_at }}</td>
<!--                                              <td> <span class="glyphicon glyphicon-remove" aria-hidden="true" onclick="alert('good')" ></span>
                                               &nbsp;<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></td></tr>-->
                                          @endforeach
                                </tbody>
                            </table>
                            <!-- ********************************************** -->




                        </div>
                    </div>
                </div>
            </section></div>






    </section>
</section>
<!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script><script src="assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script><script src="assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script><script src="assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@stop