<?php
/**
 * Created by PhpStorm.
 * User: Owden
 * Date: 01/09/2016
 * Time: 12:47 PM
 */?>
@include('admin.layouts.header')
<!-- BEGIN BODY -->
<body class=" "><!-- START TOPBAR -->
<div class='page-topbar '>
    <div class='logo-area'>

    </div>
    <div class='quick-area'>
        <div class='pull-left'>
            <ul class="info-menu left-links list-inline list-unstyled">
                <li class="sidebar-toggle-wrap">
                    <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="message-toggle-wrapper" >
                    </li>
             

                    </ul>
                </li>
            
            </ul>
        </div>
        <div class='pull-right'>
            <ul class="info-menu right-links list-inline list-unstyled">
                <li class="profile">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <img src="{{url('')}}/admin_assets/data/profile/avatar.png" alt="user-image" class="img-circle img-inline">
                        <span> {{ Auth::user()->name }} <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                        <li class="last">
                            <a  href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                <i class="fa fa-lock"></i>
                                Logout
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</div>
<!-- END TOPBAR -->
<!-- START CONTAINER -->
<div class="page-container row-fluid container-fluid">

    <!-- SIDEBAR - START -->

    <div class="page-sidebar pagescroll">

        <!-- MAIN MENU - START -->
        <div class="page-sidebar-wrapper" id="main-menu-wrapper">

            <!-- USER INFO - START -->
            <div class="profile-info row">

                <div class="profile-image col-xs-4">
                    <a href="#">
                        <img alt="" src="{{url('')}}/admin_assets/data/profile/avatar.png" class="img-responsive img-circle">
                    </a>
                </div>

                <div class="profile-details col-xs-8">

                    <h3>
                        <a href="#"> {{ Auth::user()->name }}</a>

                        <!-- Available statuses: online, idle, busy, away and offline -->
                        <span class="profile-status online"></span>
                    </h3>

                    <p class="profile-title">BNN Admin</p>

                </div>

            </div>
            <!-- USER INFO - END -->



            <ul class='wraplist'>

                <li class='menusection'>Main</li>
                <li class="open">
                    <a href="{!!url('/admin')!!}">
                        <i class="fa fa-dashboard"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-group"></i>
                        <span class="title">Affilliates</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
<!--                         <li>
                            <a class="" href="{!!url('/admin/select_affilliate')!!}" >All Affilliate</a>
                        </li> -->
                        
                        <li>
                            <a class="" href="{!!url('/admin/select_affilliate')!!}/add" >Add Affilliate</a>
                        </li>
                        <li>
                            <a class="" href="{!!url('/admin/select_affilliate')!!}/edit" >Edit Affilliate</a>
                        </li>
                       
                   
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Reservations</span>
                    </a>
<!--                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{!!url('/admin/inbox')!!}" >Inbox</a>
                        </li>
                        <li>
                            <a class="" href="{!!url('/admin/edit/compose')!!}" >Compose</a>
                        </li>
                        <li>
                            <a class="" href="{!!url('/admin/reservation')!!}" >View</a>
                        </li>
                    </ul>-->
                </li>
                <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-upload"></i>
                    <span class="title">File Uploader</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                            <a class="" href="{!!url('/admin/add/add_gallery')!!}">Add to Gallery</a>
                            </li>
                            <li>
                            <a class="" href="{!!url('/admin/all_gallery')!!}/g">All Gallery</a>
                            </li>
                        </ul>
                    </li>
        
                      <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">News&Events</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                            <a class=""href="{!!url('/admin/all_news')!!}/n">All News</a>
                            </li>
                            <li>
                            <a class="" href="{!!url('/admin/add/add_news')!!}" >Add News/event</a>
                            </li>
<!--                            <li>
                            <a class="" href="{!!url('/admin/update_news')!!}" >Edit News/event</a>
                            </li>-->
                        </ul>
                    </li>
       
              <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span class="title">Users</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                            <a class="" href="{!!url('/admin/all_user')!!}" >All Users</a>
                            </li>
                            <li>
                            <a class="" href="{!!url('/admin/add/add_user')!!}" >Add User</a>
                            </li>
                        </ul>
                    </li>
                         <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Contact Us</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                            <a class="" href="{!!url('/admin/add/add_contact')!!}" >Add content</a>
                            </li>
                            <li>
                            <a class="" href="{!!url('/admin/edit_contact')!!}" >Edit Content</a>
                            </li>                        
                        </ul>
                    </li>
                       <li class=""> 
                    <a   href="{!!url('/admin/feedback')!!}">
                    <i class="fa fa-envelope-square"></i>
                    <span class="title">Feedbacks</span>
                        </a>
                    </li>
                     <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-bars"></i>
                    <span class="title">About Us</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                            <a class=""  href="{!!url('/admin/add/add_about')!!}" >Add content</a>
                            </li>
                            <li>
                            <a class="" href="{!!url('/admin/edit_about')!!}" >Edit Content</a>
                            </li>                        
                        </ul>
                    </li>

            </ul>

        </div>
        <!-- MAIN MENU - END -->
    </div>
    <!--  SIDEBAR - END -->
@yield('content')

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


<!-- CORE JS FRAMEWORK - START -->
<script src="{{url('/')}}/admin_assets/assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
<script>window.jQuery||document.write('<script src="{{url('/')}}/admin_assets/assets/js/jquery-1.11.2.min.js"><\/script>');</script>

<!-- CORE JS FRAMEWORK - END -->
<script src="{{url('/')}}/admin_assets/assets/js/form.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/js/jquery.form.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/admin_assets/jalert/jAlert.min.js"></script>
<script src="{{url('/')}}/admin_assets/jalert/jAlert-functions.min.js"> //optional!!</script>

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

@yield('specific_js')
<!--
<script src="{{url('/')}}/admin_assets/assets/plugins/jvectormap/jquery-jvectormap-2.0.1.min.js" type="text/javascript"></script><script src="{{url('/')}}/admin_assets/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script><script src="{{url('/')}}/admin_assets/assets/js/dashboard.js" type="text/javascript"></script><script src="{{url('/')}}/admin_assets/assets/plugins/echarts/echarts-custom-for-dashboard.js" type="text/javascript"></script> OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

<!-- CORE TEMPLATE JS - START -->
<script src="{{ url('/') }}/admin_assets/assets/js/scripts.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS - END -->

@include('admin.layouts.footer')
    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    </script>