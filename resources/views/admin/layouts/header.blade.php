<?php
/**
 * Created by PhpStorm.
 * User: Owden
 * Date: 01/09/2016
 * Time: 12:50 PM
 */
?>
<!DOCTYPE html>
<html class=" ">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>BNN ADMIN: Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="<?=  asset_url()?>images/favicon.png" type="image/x-icon" />    <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="<?=  asset_url()?>images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=  asset_url()?>images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=  asset_url()?>images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=  asset_url()?>images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="<?=  asset_url()?>plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?=  asset_url()?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=  asset_url()?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=  asset_url()?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?=  asset_url()?>css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=  asset_url()?>plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS FRAMEWORK - END -->
        <!-- CORE CSS TEMPLATE - START -->
        <link href="<?=  asset_url()?>css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?=  asset_url()?>css/responsive.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="{{url('/')}}/admin_assets/jalert/jAlert.css" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- CORE CSS TEMPLATE - END -->

    </head>

    <!-- END HEAD -->