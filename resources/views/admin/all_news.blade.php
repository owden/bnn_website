<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')
@section('content')
   <section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">All News and Events</h1> 
            </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>

<div class="col-lg-12">
     @foreach($all_news as $value)
    <section class="box ">
            <div class="content-body">    <div class="row">
        <div class="col-xs-12">


            <!-- start -->
           

            <div class="blog_post">
                <h3><a href="#">{{ $value->title }}</a></h3>
                <h5>Written on  {{ $value->created_at }}.</h5>
                <img class="" style="max-width: 800px;height: auto;width:100%;margin:30px 0;" src="{{ $value->path }}/{{ $value->name }}" alt="">
                   {!!$value->content !!}
                   <div class="col-xs-12" id='success{{ $value->webcontent_id }}' hidden="true">
                   <div class="alert alert-success alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Success:</strong> The News/Event is successfully deleted.</div>
                        </div>
                   <div class="col-xs-12" id='error{{ $value->webcontent_id }}' hidden="true">
                   <div class="alert alert-error alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Danger:</strong> There is an Error,please try again.
                   </div></div>
        <div class="col-xs-8 col-md-4">
            
            <a href="{!!url('/admin/edit_single_news/')!!}/{{ $value->webcontent_id }}/type/n">  <button type="button" class="btn btn-warning"><i class="fa fa-pencil">Edit</i></button></a>
                    <button type="button" class="btn btn-danger" onclick="edit_news('{{ $value->webcontent_id }}','n')"><i class="fa fa-remove">Delete</i></button>
                </div>
                   
            </div>


 </div>
    </div>
    </div>
    </section>
            @endforeach
</div>
    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')

<script type="text/javascript">
         var siteURL="{{url('/')}}";    
           function edit_news(webcontent_id,action) {
               var id=webcontent_id;
            $.post(siteURL+"/edit_news", {webcontent_id: webcontent_id,action:action}, function (data) {
               if(data.success=='1') {
                       $('#success'+id).show().fadeOut(4000, function () {
    
    window.location = siteURL + "/admin/all_news/n";
          });
                   } else {
                     $('#error'+id).show().fadeOut(4000, function () {
    
    
          });;   
                   }
              // location.reload();  
        
                //NProgress.done();
            },'json');
   
    };
    
</script>
@stop