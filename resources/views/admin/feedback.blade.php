<?php ?>
@extends('admin.layouts.master')
@section('content') 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link href="<?=  asset_url()?>plugins/icheck/skins/minimal/minimal.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>
        <div class='col-xs-12'>
            <div class="page-title">

                <div class="pull-left">
                    <h1 class="title">FEEDBACKS</h1>                            </div>

                <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!!url('/')!!}"><i class="fa fa-home"></i>Visit Website</a>
                        </li>
                       
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box nobox ">
                <div class="content-body">    <div class="row">


                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="mail_content">

                                <div class="row">
                                 <div class="mail_list col-xs-12">
                                        <div id="result"></div>

                                        <table class="display table table-hover">
                                            @foreach($feedback_data as $value)
                                            <tr class="unread" id="mail_msg_1"  >
                                                <td class=""><div class="star">
                                                        @if($value->isread ==0)
                                                        <i class='fa fa-star-o icon-xs icon-accent' onclick="feedback('{{ $value->feedback_id }}',1)"></i>
                                                        @elseif($value->isread==1)
                                                        <i class='fa fa-star icon-xs icon-accent'></i>
                                                        @endif   
                                                    </div></td>
                                                <td>{{ $value->email }}</td>
                                                <td><span class="msgtext">{{ $value->comment }}.</span></td>
                                                <td><span class="msg_time">{{ $value->created_at }}</span></td>
                                                <td><span class="glyphicon glyphicon-remove" aria-hidden="true" onclick="feedback('{{ $value->feedback_id }}',0)" ></span>
                                               </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </section></div>


    </section>
</section>
<!-- END CONTENT -->

@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@stop
<script type="text/javascript">
         var siteURL="{{url('/')}}/read_feedback";    
           function feedback(feedback_id,action) {
         //NProgress.start();
            $('#'+feedback_id).html(window.LOADER);
            $.post(siteURL, {feedback_id: feedback_id,action:action}, function (data) {
                $('#result').html(data.message).fadeOut(4000, function () {
               location.reload();  
            });
                //NProgress.done();
            },'json');
   
    };
</script>