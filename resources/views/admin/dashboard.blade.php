<?php
/**
 * Created by PhpStorm.
 * User: Owden
 * Date: 01/09/2016
 * Time: 1:07 PM
 */?>
@extends('admin.layouts.master')
@section('content')

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="<?=  asset_url()?>plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


            <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">BNN ADMIN PANEL</h1>                            </div>

                                
        </div>
    </div>
    <div class="clearfix"></div>
    

<div class="col-lg-12">
    <section class="box nobox ">
                <div class="content-body">

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <i class='pull-left fa fa-thumbs-up icon-md icon-rounded icon-primary'></i>
                <div class="stats">
                    <?php
              $five= \App\Models\Rating::where('count',5)->count()*100;
               $four= \App\Models\Rating::where('count',4)->count()*100;
                $three= \App\Models\Rating::where('count',3)->count()*100;
                 $two= \App\Models\Rating::where('count',2)->count()*100;
                  $one= \App\Models\Rating::where('count',1)->count()*100;
                  if(empty($total_rating)){
                  $total_rating=1;    
                  }  
            
            ?>
                    <h4><strong>{{floor($five/$total_rating)}}%</strong></h4>
                    <span>Five star</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <i class='pull-left fa fa-user icon-md icon-rounded icon-accent'></i>
                <div class="stats">
                   <h4><strong>{{floor($four/$total_rating)}}%</strong></h4>
                    <span>Four Star</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <i class='pull-left fa fa-database icon-md icon-rounded icon-purple'></i>
                <div class="stats">
                 <h4><strong>{{floor($three/$total_rating)}}%</strong></h4>
                    <span>Three Star</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <i class='pull-left fa fa-users icon-md icon-rounded icon-warning'></i>
                <div class="stats">
                   <h4><strong>{{floor($two/$total_rating)}}%</strong></h4>
                    <span>Two Star</span>
                </div>
            </div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <i class='pull-left fa fa-users icon-md icon-rounded icon-warning'></i>
                <div class="stats">
                 <h4><strong>{{floor($one/$total_rating)}}%</strong></h4>
                    <span>One Star</span>
                </div>
            </div>
        </div>
    </div> <!-- End .row -->    




    </div>
        </section></div>



    </section>
    </section>
    <!-- END CONTENT -->
@stop
@section('specific_js')

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
@stop
