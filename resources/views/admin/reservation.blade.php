    <?php
?>
@extends('admin.layouts.master')
@section('content') 
       <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Mailbox</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="mail-inbox.html">Mailbox</a>
                        </li>
                        <li class="active">
                            <strong>View</strong>
                        </li>
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    

<div class="col-lg-12">
    <section class="box nobox ">
                <div class="content-body">    <div class="row">

        <div class="col-md-3  col-sm-4 col-xs-12">
            

    <a class="btn btn-primary btn-block" href='mail-compose.html'>Compose</a>

    <ul class="list-unstyled mail_tabs">
        <li class="active">
            <a href="mail-inbox.html">
                <i class="fa fa-inbox"></i> Inbox <span class="badge badge-primary pull-right">6</span>
            </a>
        </li>
        <li class="">
            <a href="mail-sent.html">
                <i class="fa fa-send-o"></i> Sent
            </a>
        </li>
        <li class="">
            <a href="mail-drafts.html">
                <i class="fa fa-edit"></i> Drafts <span class="badge badge-accent pull-right">2</span>
            </a>
        </li>
        <li class="">
            <a href="mail-important.html">
                <i class="fa fa-star-o"></i> Important
            </a>
        </li>
        <li class="">
            <a href="mail-trash.html">
                <i class="fa fa-trash-o"></i> Trash
            </a>
        </li>
    </ul>

            </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="mail_content">

                <div class="row">
                    <div class="col-xs-12">
                        
    <h3 class="mail_head">View</h3>
    <i class='fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-search icon-primary icon-xs icon-accent mail_head_icon'></i>
    <i class='fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right'></i>


                        </div>

                    <div class="col-xs-12 mail_view_title">

                        <div class="pull-left">
                            <h3 class="">New Project Alloted to your team.</h3>
                        </div>

                        <div class='pull-right'>
                            <button type="button" class="btn btn-default btn-icon" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Reply" data-placement="top">
                                <i class="fa fa-reply icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Reply All" data-placement="top">
                                <i class="fa fa-reply-all icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Forward" data-placement="top">
                                <i class="fa fa-mail-forward icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Attachments" data-placement="top">
                                <i class="fa fa-paperclip icon-xs"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-icon" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Trash" data-placement="top">
                                <i class="fa fa-trash-o icon-xs"></i>
                            </button>
                        </div>

                    </div>

                    <div class="col-xs-12 mail_view_info">

                        <div class="pull-left">
                            <span class=""><strong>Bruce Wayne</strong> (brucewayne@example.com) to <strong>You</strong></span>
                        </div>

                        <div class='pull-right'>
                            <span class='msg_ts text-muted'>12:40 PM, Today</span>
                        </div>

                    </div>



                    <div class="col-xs-12 mail_view">
                        <p>Hello Jason,</p>
                        <h4>What is Graphic Design?</h4>
                        <p>Graphic design is the art of visual communication through the use of images, words, and ideas to give information to the viewers. Graphic design can be used for advertising, or just for entertainment intended for the mind.</p>
                        <br>
                        <h4>Balance</h4>
                        <p>Designs in balance (or equilibrium) have their parts arrangement planned, keeping a coherent visual pattern (color, shape, space). "Balance" is a concept based on human perception and the complex nature of the human senses of weight and proportion. Humans can evaluate these visual elements in several situations to find a sense of balance. A design composition does not have to be symmetrical or linear to be considered balanced, the balance is global to all elements even the absence of content. In this context perfectly symmetrical and linear compositions are not necessarily balanced and so asymmetrical or radial distributions of text and graphic elements can achieve balance in a composition.</p>
                        <br>
                        <h4>Contrast</h4>
                        <p>Distinguishing by comparing/creating differences. Some ways of creating contrast among elements in the design include using contrasting colors, sizes, shapes, locations, or relationships. For text, contrast is achieved by mixing serif and sans-serif on the page, by using very different type styles, or by using type in surprising or unusual ways. Another way to describe contrast, is to say "a small object next to a large object will look smaller". As contrast in size diminishes, monotony is approached.</p>
                        <br>
                    </div>


                    <div class="col-xs-12 mail_view_attach">
                        <h3>
                            <i class="fa fa-paperclip icon-sm"></i> Attachments
                        </h3><br>

                        <ul class="list-unstyled list-inline">
                            <li>
                                <a href="#" class="file">
                                    <img alt="" src="data/mail/attach-1.png" class="img-thumbnail">
                                </a>

                                <a href="#" class="title">
                                    Project_details.jpg
                                    <span>10KB</span>
                                </a>

                                <div class="actions">
                                    <a href="#">View</a> - 
                                    <a href="#">Download</a>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="file">
                                    <img alt="" src="data/mail/attach-2.png" class="img-thumbnail">
                                </a>

                                <a href="#" class="title">
                                    Guidlines.jpg
                                    <span>14KB</span>
                                </a>

                                <div class="actions">
                                    <a href="#">View</a> - 
                                    <a href="#">Download</a>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="file">
                                    <img alt="" src="data/mail/attach-3.png" class="img-thumbnail">
                                </a>

                                <a href="#" class="title">
                                    Team_info.jpg
                                    <span>12KB</span>
                                </a>

                                <div class="actions">
                                    <a href="#">View</a> - 
                                    <a href="#">Download</a>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <div class="col-xs-12 mail_view_reply">
                        <div class="form-group">
                            <div class="controls">
                                <textarea class="form-control autogrow" cols="5" id="field-7" placeholder="Reply or Forward this message" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 120px;"></textarea>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    </div>
        </section></div>


    </section>
    </section>
       <!-- END CONTENT -->
    @stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="{{url('/')}}/admin_assets/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@stop