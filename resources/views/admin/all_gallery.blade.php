<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('admin.layouts.master')
@section('content')
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <link href="<?=  asset_url()?>plugins/prettyphoto/prettyPhoto.css" rel="stylesheet" type="text/css" media="screen"/>    
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

 

<!--  SIDEBAR - END -->
    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Gallery </h1>  </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Visit Website</a>
                        </li>                  
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    
<div class="col-lg-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left"></h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
        <div class="col-xs-12">





            <!-- start -->
            <div id='portfolio' class="outside">

                <div class="portfolio-items row">
                    @foreach($all_gallery as $value)
                    <div class="portfolio-item col-md-4 col-sm-6 col-xs-12  creative">
                        <div class="portfolio-item-inner">
                            <img class="img-responsive" src="{{ $value->path }}/{{ $value->name }}" alt="">
                            <div class="portfolio-info ">
                                <span class='desc'>{{ $value->title }}.</span>
                                <span class='img-responsive'>
                                    <a href="{!!url('/admin/edit_single_news/')!!}/{{ $value->webcontent_id }}/type/g">   <button type="button" class="btn btn-warning">
                                     <i class="fa fa-pencil">Edit</i></button></a>
                                      <button type="button" class="btn btn-danger" onclick="delete_gallery('{{ $value->webcontent_id }}','g')"><i class="fa fa-remove">Delete</i> </button>
                
                                    
                                </span>
                                <a class="preview" href="{{ $value->path }}/{{ $value->name }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
 @endforeach
                 
                </div>
            </div>
            <!-- end -->








        </div>
    </div>
    </div>
        </section></div>


    </section>
    </section>
        <!-- END CONTENT -->
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/prettyphoto/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/isotope/jquery.isotope.min.js" type="text/javascript"></script>
<script src="<?=  asset_url()?>plugins/isotope/jquery.isotope.sloppy-masonry.min.js" type="text/javascript">
</script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<script type="text/javascript">

        var siteURL="{{url('/')}}";    
           function delete_gallery(webcontent_id,type) {
               var id=webcontent_id;
            $.post(siteURL+"/edit_news", {webcontent_id: webcontent_id,type:type}, function (data) {
               if(data.success=='1') {
                       $.jAlert({
        'title': 'UPLOAD!',
        'content': 'Thank you for uploading!',
        'theme': 'green',
        'btns': { 'text': 'close' }
      });
    
    window.location = siteURL + "/admin/all_gallery/g";
        
                   } else {
                   $.jAlert({
        'title': 'Ooops!',
        'content': 'Image not uploaded!',
        'theme': 'red',
        'btns': { 'text': 'close' }
      });
                   }
              // location.reload();  
        
                //NProgress.done();
            },'json');
   
    };	
    edit_gallery=function(id){
       window.location = siteURL + "/admin/edit_single_news/"+id+"/type/g";  
    }
        </script>
@stop