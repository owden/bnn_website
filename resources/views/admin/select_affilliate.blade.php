<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
@extends('admin.layouts.master')
@section('content')
       
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link href="<?=  asset_url()?>plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?=  asset_url()?>plugins/select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>        
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
       <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
 <link href="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">SELECT AFFILLIATE</h1>                            </div>

                            <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                      
                    </ol>
                </div>
                                
        </div>
    </div>
    <div class="clearfix"></div>
    
<div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left"></h2>
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">   
                <div class="row">
        <div class="col-md-8 col-sm-9 col-xs-10">
            <div class="form-group">
                <label class="form-label">Edit Affilliate</label>
                <select class="" id="s2example-1">
                    <option></option>
                    @foreach($region as $value)
                    <optgroup label="{{$value->name}}">
                        
                        <?php
                        $sub_list=  App\Models\Webpart::where('parent_id',$value->webpart_id)->get();
                        foreach ($sub_list as $sub){?>
                           <option value="{{$sub->webpart_id}}">{{$sub->name}}</option>   
                       <?php }?>
                    </optgroup>
                     @endforeach
                </select>

 
            </div>
               <div class="form-group">
                   <button type="button" class="btn btn-primary" onmousedown="edit_affilliate()">Go</button>


            </div>

        </div>
    </div>
    </div>
        </section></div>
  <div class="col-lg-12" id="result">
   
</div>


    </section>
    </section>
@stop
@section('specific_js')
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?=  asset_url()?>plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/select2/select2.min.js" type="text/javascript"></script> 
<script src="<?=  asset_url()?>plugins/bootstrap3-wysihtml5/js/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<script type="text/javascript">
         var siteURL="{{url('/')}}";    
           function edit_affilliate() {
               var id=$('#s2example-1').val();
            window.location = siteURL+"/admin/this_affilliate/" + id;
   
    };
    
   
    
</script>
@stop