# BNN Group Website
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](http://inetstz.com/licence/website)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for this site can be found on the [BNN website](http://bnn.com/docs).

## Feedback

Thank you for considering giving feedback to the BNN website!

## Security Vulnerabilities

If you discover a security vulnerability within BNN Website, please send an e-mail to INETS Co Ltd at support@inetstz.com. All security vulnerabilities will be promptly addressed.

## License

The BNN website is  licensed under INETS Co Ltd user Licence.