<?php

/**
 * Contain all reusable files used in this application
 * @author Erick Chrysostom <gwanchi@gmail.com>
 */
if (!function_exists('clean_string')) {

    /**
     * @description replace all extra white spaces within a text with a single space and remove all the trailing spaces
     * @param type $string string to be cleaned
     * @return type cleaned string
     */
    function clean_string($string) {
	return preg_replace("/[^\p{L}|\p{N}]+/u", " ", trim($string));
    }

}

if (!function_exists('file_dir')) {

    function file_dir() {
	return public_path() . '/storage/file';
    }

}

if (!function_exists('attachment_dir')) {

    function attachment_dir() {
	return public_path() . '/storage/attachment';
    }

}

if (!function_exists('file_path')) {

    function file_path() {
	//return asset('storage/file');
	//url("/") . '/data/file';
	$uri = urldecode(
		parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
	);
	if (substr($uri, 0, 7) === '/public') {
	    return asset('/storage/file');
	} else {
	    return asset('/public/storage/file');
	}
    }

}

if (!function_exists('base_url')) {

    /**
     * @description return the base url of the application
     * @return type string base url
     */
    function base_url() {
	$uri = urldecode(
		parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
	);
	if (substr($uri, 0, 7) === '/public') {
	    return url("/");
	} else {
	    return url("/");
	}
    }

}

if (!function_exists('asset_url')) {

    /**
     * @description return the assets url of this application
     * @return type string assets url location
     */
    function asset_url() {
	$uri = urldecode(
		parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
	);
	if (substr($uri, 0, 7) ==='/public') {
	    return url("/") . 'admin_assets/assets/';
	} else {
	    return url("/") . '/admin_assets/assets/';
	}
    }

}

if (!function_exists('ip_is_private')) {

    /**
     * @description Detect whether ip is private or not, return true is the IP is private and false otherwise.
     * @param type $ip IP Address
     * @param type token extra parameter to specify that if it is the private ip from this application only
     * @return boolean
     */
    function ip_is_private($ip, $token = null) {
	$pri_addrs = array(
	    '10.0.0.0|10.255.255.255',
	    '172.16.0.0|172.31.255.255',
	    '192.168.0.0|192.168.255.255',
	    '169.254.0.0|169.254.255.255',
	    '127.0.0.0|127.255.255.255',
	    '::1|::1'
	);

	$long_ip = ip2long($ip);
	if ($long_ip != -1) {
	    foreach ($pri_addrs AS $pri_addr) {
		list($start, $end) = explode('|', $pri_addr);
		// IF IS PRIVATE
		if ($long_ip >= ip2long($start) && $long_ip <= ip2long($end))
		    return (TRUE);
	    }
	}
	return (FALSE);
    }

}

if (!function_exists('get_from_file')) {

    /**
     * @description get the value of the string part from a line in a file
     * i.e port=5,  $text is port= then return is 5.
     * @param type $text part of string which its value is to be extracted
     * @param type $file which the text is to be searched
     * @return extracted string
     */
    function get_from_file($text, $file) {
	$open_file = fopen($file, 'r');
	while ($line = fgets($open_file)) { /* Read file line by line */
	    if (strpos($line, $text) === 0) { /* Check if '$text' is at start of string */
		$output = str_replace($text, '', $line);
		break;
	    }
	}
	return $output;
    }

}

if (!function_exists('save_to_file')) {

    /**
     * @description replace the string part with the new value
     * i.e port=5, text is port=, value is 6, the value in the file will be port=6
     * @param type $text part of the text which its value is to be saved
     * @param type $file which the text is to be saved
     * @param type $value new value for the text
     */
    function save_to_file($text, $file, $value) {
	$open_file = fopen($file, 'r');
	while ($line = fgets($open_file)) { /* Read file line by line */
	    if (strpos($line, $text) === 0) { /* Check if '$text' is at start of string */
		$new_value = $text . $value;
		$file_contents = file_get_contents($file); /* get the content of the file */
		$file_contents = str_replace($line, $new_value . "\n", $file_contents);
		file_put_contents($file, $file_contents);
		break;
	    }
	}
    }

}

if (!function_exists('get_db_param')) {

    /**
     * @description use the function get_from_file to get the value of the string par
     * @param type $input string part to extract information
     * @return extracted information
     */
    function get_db_param($input) {
	$file = base_path() . '/.env';
	return get_from_file("{$input}=", $file);
    }

}

if (!function_exists('save_db_param')) {

    /**
     * @description use the function save_to_file to replace the string part with the new value
     * @param type $input parameter to saved in the database connection file
     */
    function save_db_param($input, $value) {
	$file = base_path() . '/.env';
	save_to_file("{$input}=", $file, $value);
    }

}

if (!function_exists('getOS')) {

    /**
     * @description detect the operating system which a server is running
     * @global type $user_agent
     * @return string os platform of the user
     */
    function getOS() {

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$os_platform = "Unknown OS Platform";

	$os_array = array(
	    '/windows nt 6.2/i' => 'Windows 8',
	    '/windows nt 6.1/i' => 'Windows 7',
	    '/windows nt 6.0/i' => 'Windows Vista',
	    '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
	    '/windows nt 5.1/i' => 'Windows XP',
	    '/windows xp/i' => 'Windows XP',
	    '/windows nt 5.0/i' => 'Windows 2000',
	    '/windows me/i' => 'Windows ME',
	    '/win98/i' => 'Windows 98',
	    '/win95/i' => 'Windows 95',
	    '/win16/i' => 'Windows 3.11',
	    '/macintosh|mac os x/i' => 'Mac OS X',
	    '/mac_powerpc/i' => 'Mac OS 9',
	    '/linux/i' => 'Linux',
	    '/ubuntu/i' => 'Ubuntu',
	    '/iphone/i' => 'iPhone',
	    '/ipod/i' => 'iPod',
	    '/ipad/i' => 'iPad',
	    '/android/i' => 'Android',
	    '/blackberry/i' => 'BlackBerry',
	    '/webos/i' => 'Mobile'
	);

	foreach ($os_array as $regex => $value) {

	    if (preg_match($regex, $user_agent)) {
		$os_platform = $value;
	    }
	}
	return $os_platform;
    }

}

if (!function_exists('getBrowser')) {

    /**
     * @description the type of browser the client is using
     * @global type $user_agent
     * @return string user browser
     */
    function getBrowser() {

	global $user_agent;

	$browser = "Unknown Browser";

	$browser_array = array(
	    '/msie/i' => 'Internet Explorer',
	    '/firefox/i' => 'Firefox',
	    '/safari/i' => 'Safari',
	    '/chrome/i' => 'Chrome',
	    '/opera/i' => 'Opera',
	    '/netscape/i' => 'Netscape',
	    '/maxthon/i' => 'Maxthon',
	    '/konqueror/i' => 'Konqueror',
	    '/mobile/i' => 'Handheld Browser'
	);

	foreach ($browser_array as $regex => $value) {

	    if (preg_match($regex, $user_agent)) {
		$browser = $value;
	    }
	}
	return $browser;
    }

}

if (!function_exists('createFile')) {

    /**
     * @description : Create a file if it does not exists and add the contents or append contents inside
     * and if contents already exist it skip
     * @param type $filename is the name of the file
     * @param type $content contents inteded to saved
     * @param type $isAppend boolean value to describe if content should appended or 
     * copied.
     */
    function createFile($filename, $content, $isAppend = true) {
	if (file_exists($filename)) {
	    $open_file = fopen($filename, 'r');
	    while ($line = fgets($open_file)) { /* Read file line by line */
		if (strpos($line, $content) === 0) { /* Check if '$text' is at start of string */
		    return;
		}
	    }
	} else {
	    fopen($filename, "w");
	    chmod($filename, 0600);
	}
	file_put_contents($filename, $content, ($isAppend) ? FILE_APPEND : NULL);
    }

}

if (!function_exists('app_name_icon')) {

    /**
     * Helper to grab the application name icon decorated
     *
     * @return mixed
     */
    function app_name_icon() {
	return config('constant.app_name_icon');
    }

}

if (!function_exists('app_name')) {

    function app_name() {
	return config('constant.app_name');
    }

}

/**
 * @description return the mail address of the system
 */
if (!function_exists('app_mail')) {

    function app_mail() {
	return config('constant.app_mail');
    }

}

if (!function_exists('company')) {

    /**
     * Helper to grab the company name
     *
     * @return mixed
     */
    function company() {
	return config('constant.company');
    }

}

if (!function_exists('access')) {

    /**
     * Access (lol) the Access:: facade as a simple function
     */
    function access() {
	return app('access');
    }

}


if (!function_exists('history')) {

    /**
     * Access the history facade anywhere
     */
    function history() {
	return app('history');
    }

}


if (!function_exists('notification')) {

    /**
     * Access the notification facade anywhere
     */
    function notification() {
	return app('notification');
    }

}

if (!function_exists('getFallbackLocale')) {

    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function getFallbackLocale() {
	return config('app.fallback_locale');
    }

}

if (!function_exists('getLanguageBlock')) {

    /**
     * Get the language block with a fallback
     *
     * @param $view
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function getLanguageBlock($view, $data = []) {
	$components = explode("lang", $view);
	$current = $components[0] . "lang." . app()->getLocale() . "." . $components[1];
	$fallback = $components[0] . "lang." . getFallbackLocale() . "." . $components[1];

	if (view()->exists($current)) {
	    return view($current, $data);
	} else {
	    return view($fallback, $data);
	}
    }

}

if (!function_exists('formatBytes')) {

    function formatBytes($size, $precision = 2) {
	if ($size > 0) {
	    $size = (int) $size;
	    $base = log($size) / log(1024);
	    $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
	    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
	} else {
	    return $size;
	}
    }

}
if (!function_exists('loader')) {

    function loader() {
        $path='<img src="'.asset_url().'images/ajax-loader.gif" alt=""/>';
	return $path;
    }

}