<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mail;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     *
     * @var email
     * This concept is used only because variables cannot be passed directly to
     * mail class so we create global value and inject them in a method
     */
    private $email;
    private $subject;
    private $message;
    private $attachment_path;

    public function send_email($email, $subject, $content, $attachment_path = NULL) {
        $this->email = $email;
        $this->subject = $subject;
        $this->message = $content;
        $this->attachment_path = $attachment_path;
        return Mail::send('email.template', ['content' => $content], function ($sendmail) {

                    if ($this->attachment_path != null) {
                        $sendmail->attach($this->attachment_path, []);
                    }
                    // Get the underlying SwiftMailer message instance...
                    //$sendmail->getSwiftMessage()->getHeaders();
                    $sendmail->to($this->email)->subject($this->subject);
                });
    }

}
