<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Reservation;
use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Webcontent;
use App\Models\Webgal;
use \App\Models\Webpart;
use App\Models\File;
use Illuminate\Cookie\CookieJar;
use App\Models\Rating;
use \Intervention\Image\Facades\Image;

class WebController extends Controller {

//    public function affiliate () {
//        $table = DB::table('webpart')->get();
//        return view('affilliate', $table);
//    }
   public function index() {
                $data = Webcontent::where('webpart_id', 1)
                        ->orderBy('webcontent_id','desc')
                        ->take(4)
                        ->get();
                return view('website.home',['slide_content'=>$data]);
    }

    public function news() {
        $data = Webcontent::where('webcontent.webpart_id', 3)
                ->join('webgal', 'webcontent.webcontent_id', '=', 'webgal.webcontent_id')
                ->join('file', 'webgal.file_id', '=', 'file.file_id')
                ->get();
        return view('website.news', ['news_content' => $data]);
    }

    public function read_single_news($id) {
        $content = Webcontent::where('webcontent_id', $id)->first();
        $other_news = Webcontent::whereRaw("webcontent_id !=$id AND webpart_id=3")->get();
        return view('website.news_description', ['singlenews_content' => $content, 'other_news' => $other_news,'menu_id'=>$id]);
    }

    public function affiliate($webpart_id) {
        $content = Webcontent::where('webpart_id', $webpart_id)->get();
        return view('website.affilliate', ['content' => $content, 'webpart_id' => $webpart_id]);
    }
    public function menu($id) {
        $static_menu = array("1", "3", "4", "5","6", "7");
        if (in_array($id, $static_menu)) {
            switch ($id):
                case 1:
                $data = Webcontent::where('webpart_id', 1)
                        ->orderBy('webcontent_id','desc')
                        ->take(4)
                        ->get();
                return view('website.home',['slide_content'=>$data]);
                case 3:
                $data = Webcontent::where('webcontent.webpart_id',3)
                        ->orderBy('webcontent_id','desc')
                        ->paginate(6);
        return view('website.news', ['news_content' => $data]);
                case 4:
               return view('website.reservation');     
                case 5:
        $data = Webcontent::where('webcontent.webpart_id',5)
                        ->orderBy('webcontent_id','desc')
                        ->paginate(6);
            return view('website.gallery', ['all_gallery' => $data]);  
                case 6:
              $content = Webcontent::where('webpart_id', 6)->get();
        return view('website.contact', ['contact_content' => $content]);
                case 7:
             $content = Webcontent::where('webpart_id', 7)->get();
        return view('website.about', ['about_content' => $content]);
          default :
         return view('website.home');  
                    
            endswitch;

}else{
    
     $content = Webcontent::where('webpart_id', $id)->get();
     return view('website.affilliate', ['content' => $content,'menu_id'=>$id]);
        
    }
    }
    public function save_contact() {
        //send the details to the email
        return redirect()->to('contact')->with('success', 'Successfully Submitted!');
    }

    public function save_reservation(Request $request) {
        $firstname=$request->input('firstname');
        $lastname=$request->input('lastname');
        $checkin_date=$request->input('checkin_date');
        $checkout_date=$request->input('checkout_date');
        $adult=$request->input('adult');
        $children=$request->input('children');
        $phonenumber=$request->input('phonenumber');
        $other=$request->input('other');
        $email=$request->input('email');
        $message='  <tr>
                                <td>Full Name </td>
                                <td>
'.$firstname.' '.$lastname.'
                                    
                                </td><br/><br/>							 
                            </tr>
                              <tr>
                                <td>Check in Date </td>
                                <td>
                                    '.$checkin_date.'
                                    
                                </td><br/><br/>							 
                            </tr>
                              <tr>
                                <td> Check out Date </td>
                                <td>
                                '.$checkout_date.'
                                    
                                </td><br/><br/>							 
                            </tr>
                              <tr>
                                <td> Adult </td>
                                <td>
                                '.$adult.'
                                    
                                </td><br/> <br/>							 
                            </tr>
                              <tr>
                                <td> Children </td>
                                <td>
                                '.$children.'
                                    
                                </td><br/><br/>							 
                            </tr>
                              <tr>
                                <td>Phonenumber </td>
                                <td>

                                  '.$phonenumber.'  
                                </td><br/><br/>							 
                            </tr>
                              <tr>
                                <td>Other </td>
                                <td>

                                  '.$other.'  
                                </td><br/><br/>							 
                            </tr>';
        $send_email=$this->send_email($email, 'Reservation Request', $message);
        if($send_email==null){
               return redirect()->to('menu/4')->with('success', 'Successfully Submitted!'); 
        
        } else{
         return redirect()->to('menu/4')->with('error', 'Failed to Submit!');      
        }
       // if ($reservation->save())
    }

    public function save_feedback() {
        $feedback = new Feedback(request()->all());
        if ($feedback->save())
            return redirect()->to('/menu/6')->with('success', 'Successfully Submitted!');
        return redirect()->to('/menu/6')->with('error', 'Failed to send Message!');
    }

    public function gallery() {
        return view('website.gallery');
    }

    //ADMIN FUNCTIONS
       public function admin_dashboard() {
        $data = Rating::all()->count();
        return view('admin.dashboard', ['total_rating' => $data]);
    }
    public function admin_feedback() {
        $data = Feedback::all();
        return view('admin.feedback', ['feedback_data' => $data]);
    }

    public function select_affilliate($action) {
        $region = Webpart::whereBetween('webpart_id', array(8, 10))->get();
        if ($action == 'edit') {
            return view('admin.select_affilliate', ['region' => $region]);
        } elseif ($action == 'add') {
            return view('admin.add.add_affilliate', ['region' => $region]);
        } else {
            
        }
    }

    public function affilliate_toedit($id) {
        $web_content = Webcontent::where('webpart_id', $id)->first();
        return view('admin.edit.edit_affilliate', ['content' => $web_content]);
    }

    public function new_affilliate(Request $request) {
        //getting all of the post data
        $image_upload = new File();
        $webcontent = new Webcontent();
        $webpart = new Webpart();
          $webgal = new Webgal();
        $webcontent->title = '';
        $webcontent->content = $request->input('content');
        $webpart->parent_id = $request->input('webpart_id');
        $webpart->websec_id = $request->input('websec_id');
        $webpart->name = $request->input('affilliate_name');
        $file = array('image' => Input::file('pic'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return response()->json(['message' => 'please try again']);
        } else {
            // checking file is valid.
            if (Input::file('pic')->isValid()) {
               
                $destinationPath ='../storage/file'; // upload path
              
                $extension = Input::file('pic')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('pic')->move($destinationPath, $fileName); // uploading file to given path 
                
//              $img = Image::make($image->getRealPath())->resize(3200, 3200);
//            
//              $img->save($destinationPath .'/'.$fileName);
                $image_upload->name = $fileName;
                $image_upload->path = url($destinationPath);
                  if ($webpart->save()) {
                      $webcontent->webpart_id = $webpart->webpart_id; 
                      
                       if ($webcontent->save() && $image_upload->save()) {
                    $webgal->webcontent_id = $webcontent->webcontent_id;
                    $webgal->file_id = $image_upload->file_id;
                    if ($webgal->save()) {
                         return response()->json(['message' => 'Affilliate is added successfully']);
                    }
                } else {
                        return response()->json(['message' => 'Affilliate is unable to be added ,please try again']);
                }
                  } else{
                          return response()->json(['message' => 'Error in saving data']);
             
                  }
                // sending back with message
               
            } else {
                // sending back with error message.
                return response()->json(['message' => 'Image uploaded is not supported try again']);
            }
        }
    }

    public function read_feedback(Request $request) {
        $id = $request->input('feedback_id');
        $action = $request->input('action');
        if ($action == 1) {
            $data = Feedback::where('feedback_id', $id)->update(['isread' => 1]);
            if ($data == 1) {
                $message = "Successfully Updated";
            } else {
                $message = "Unable to Update";
            }
        } else {
            $remove = Feedback::where('feedback_id', $id)->delete();
            if ($remove == 1) {
                $message = "Successfully deleted";
            } else {
                $message = "Unable to delete";
            }
        }
        return response()->json(['message' => $message]);
    }

    public function add_user(Request $request) {
       $validator = Validator::make(Input::all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        
        if($validator->fails()){
         return response()->json(['message' => 'User is unable to be added ,please try again']);   
        }else {
            $user=new User();
             $user->name=trim($request->input('name'));
             $user->email=trim($request->input('email'));
             $user->password=  bcrypt(trim($request->input('password')));
      
        if ($user->save()) {
            return response()->json(['message' => 'User is added successfully']);
        } 
    }
    }
    public function add_affilliate() {
        $user = new User(request()->all());
        if ($user->save()) {
            return response()->json(['message' => 'User is added successfully']);
        } else {
            return response()->json(['message' => 'User is unable to be added ,please try again']);
        }
    }

    public function all_user() {
        $data = User::all();
        return view('admin.all_user', ['all_user' => $data]);
    }

    public function all_news($type) {
//         $data = Webcontent::join('webgal', 'webcontent.webcontent_id', '=', 'webgal.webcontent_id')
//            ->join('file','webgal.file_id','=','file.file_id')
//            ->get();

        if ($type == 'g') {

            $data = Webcontent::where('webcontent.webpart_id', '!=', 3)
                    ->join('webgal', 'webcontent.webcontent_id', '=', 'webgal.webcontent_id')
                    ->join('file', 'webgal.file_id', '=', 'file.file_id')
                    ->orderBy('file.file_id','desc')
                    ->get();
            return view('admin.all_gallery', ['all_gallery' => $data]);
        } else {
            $data = Webcontent::where('webcontent.webpart_id', 3)
                    ->join('webgal', 'webcontent.webcontent_id', '=', 'webgal.webcontent_id')
                    ->join('file', 'webgal.file_id', '=', 'file.file_id')
                    ->orderBy('file.file_id','desc')
                    ->get();
            return view('admin.all_news', ['all_news' => $data]);
        }
    }

    public function edit_single_news($id, $type) {

        //update that specific news or event   
        $content = Webcontent::where('webcontent.webcontent_id', $id)
                ->join('webgal', 'webcontent.webcontent_id', '=', 'webgal.webcontent_id')
                ->join('file', 'webgal.file_id', '=', 'file.file_id')
                ->get();
        if ($type == 'n') {
            return view('admin.edit.edit_news', ['edit_single_news' => $content]);
        } elseif ($type == 'g') {
            return view('admin.edit.edit_gallery', ['edit_single_gallery' => $content]);
        }
    }

    public function update_affilliate(Request $request) {
        $content = trim($request->input('content'));
        $id = $request->input('webcontent_id');
        if(!empty($content)){
        $data = Webcontent::where('webcontent_id', $id)->update(['content' => $content, 'title' => '']);
        
         if ($data == 1) {
            return response()->json(['message' => 'Information Updated successfully']);
        } else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }} else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }
    }

    public function update_about(Request $request) {
        $content = trim($request->input('editor1'));
        $id = $request->input('webcontent_id');
        if(!empty($content)){
               $data = Webcontent::where('webcontent_id', $id)->update(['content' => $content, 'title' => 'ABOUT US']);
             if ($data == 1) {
            return response()->json(['message' => 'Information Updated successfully']);
        } else {
            return response()->json(['message' => 'Information not Updated ,try again later']);
        }
        } else {
            return response()->json(['message' => 'Information not Updated ,try again later']);
        }
    }

    public function update_contact(Request $request) {
        $content = trim($request->input('editor1'));
        $id = $request->input('webcontent_id');
        if(!empty($content)){
        $data = Webcontent::where('webcontent_id', $id)->update(['content' => $content]);
         if ($data == 1) {
            return response()->json(['message' => 'Information Updated successfully']);
        } else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }    
        }else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }
    }

    public function edit_news(Request $request) {
        $webcontent_id = $request->input('webcontent_id');
        //$remove=DB::table('webcontent')->where('webcontent_id', '>', $webcontent_id)->delete();
        $remove = Webcontent::where('webcontent_id', $webcontent_id)->delete();
        //  dd($remove);
        if ($remove == 1) {
            return response()->json(['success' => "1"]);
        } else {
            return response()->json(['success' => "0"]);
        }

        //return view('admin.all_news',['all_news'=>$content]);
    }

    public function edit_about() {

        $content = Webcontent::where('webpart_id', 7)->get();
        return view('admin.edit.edit_about', ['update_about' => $content]);
    }

    public function edit_contact() {
        $content = Webcontent::where('webpart_id', 6)->get();
        return view('admin.edit.edit_contact', ['update_contact' => $content]);
    }

    public function add_contact(Request $request) {
        $webcontent = new Webcontent();
        $webcontent->content = $request->input('editor1');
        $webcontent->title = "CONTACT US";
        $webcontent->webpart_id = 6;
        if ($webcontent->save()) {
            return response()->json(['message' => 'Information Submitted']);
        } else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }
    }

    public function add_about(Request $request) {
             
        //$webcontent->content=$request->input('editor1');
        //$webcontent->title=$request->input('About Us');
        //$webcontent->webpart_id=7;
        $content = $request->input('content');
        if(empty(Webcontent::find(7))){
             $webcontent=new Webcontent(); 
             $webcontent->title=$request->input('About Us');
             $webcontent->content=$content;
             $webcontent->webpart_id=7;
             $data= $webcontent->save();
               if ($data == 1) {
            return response()->json(['message' => 'Information Inserted successfully']);
        } else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }
            
        } else{
        $data = Webcontent::where('webpart_id', 7)->update(['title' => 'About Us', 'content' => $content]);
        if ($data == 1) {
            return response()->json(['message' => 'Information Updated successfully']);
        } else {
            return response()->json(['message' => 'Information not Submitted ,try again later']);
        }
    } }

    public function upload_file(Request $request) {
        $image_upload = new File();
        $webcontent = new Webcontent();
        $webgal = new Webgal();
        // getting all of the post data
        $id = $request->input('webpart_id');
        // $image_upload->type=$type;
        $webcontent->content = $request->input('content');
        $webcontent->title = $request->input('title');
        $webcontent->webpart_id = $id;
        $file = array('image' => Input::file('pic'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return response()->json(['message' => 'please try again']);
        } else {
            // checking file is valid.
            if (Input::file('pic')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('pic')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('pic')->move($destinationPath, $fileName); // uploading file to given path
                $image_upload->name = $fileName;
                $image_upload->path = url($destinationPath);
                // sending back with message
                if ($webcontent->save() && $image_upload->save()) {
                    $webgal->webcontent_id = $webcontent->webcontent_id;
                    $webgal->file_id = $image_upload->file_id;
                    if ($webgal->save()) {
                        return response()->json(['message' => 'Submitted successfully']);
                    }
                } else {
                    return response()->json(['message' => 'Error in Submitting data']);
                }
            } else {
                // sending back with error message.
                return response()->json(['message' => 'please try again']);
            }
        }
    }

    public function update_news(Request $request) {
        $image_upload = new File();
        $webcontent = new Webcontent();
        $webgal = new Webgal();
        // getting all of the post data
        $title = $request->input('title');
        $content = $request->input('content');
        $content_id = $request->input('webcontent_id');
        $file = array('image' => Input::file('pic'));
        // setting up rules
        $rules = array('image' => 'required | mimes:jpeg,jpg,png | max:1000'); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return response()->json(['message' => 'File uploaded is not supported']);
        } else {
            // checking file is valid.
            if (Input::file('pic')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('pic')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('pic')->move($destinationPath, $fileName); // uploading file to given path
                $image_upload->name = $fileName;
                $image_upload->path = url($destinationPath);
                $update_content=Webcontent::where('webcontent_id', $content_id)->update(['title'=>$title,'content'=>$content]); 
                $file_info=  Webgal::where('webcontent_id',$content_id)->first();
                $file_id=$file_info->file->file_id;
                $update_file=  File::where('file_id',$file_id)->update(['name'=>$fileName,'path'=>url($destinationPath)]);
             
               
                //// sending back with message
                //update file path,content,title etc
                if ($update_file ===1 && $update_content===1) {

                    return response()->json(['message' => 'Your news and Events has been submitted successfully']);
                } else {
                    return response()->json(['message' => 'Error in Submitting data']);
                }
            } else {
                // sending back with error message.
                return response()->json(['message' => 'You have not uploaded the correct file']);
            }
        }
    }
    public function rating (Request $request){
        $rate_value= $request->input('rating');
        $rate=new Rating();
        $rate->count=$rate_value;
        if($rate->save()){
         return response()->json(['message' => 'Thank you for rating']);   
        }    
    }
    
    
 public function isValid (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

      return $validator;

        // Store the blog post...
    }

}
