<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webgal extends Model {

    /**
     * Generated
     */

    protected $table = 'webgal';
     protected $primaryKey ='webgal_id'; 
    protected $fillable = ['webcontent_id', 'file_id', 'webgal_id'];


    public function webcontent() {
        return $this->belongsTo(\App\Models\Webcontent::class, 'webcontent_id', 'webcontent_id');
    }
    
    public function file() {
        return $this->hasOne(\App\Models\File::class, 'file_id', 'file_id');
    }


}
