<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    /**
     * Generated
     */

    protected $table = 'feedback';
     protected $primaryKey ='feedback_id';
    protected $fillable = ['name', 'email', 'phone_number', 'country', 'comment','subject', 'isread', 'feedback_id'];



}
