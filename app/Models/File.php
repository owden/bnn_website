<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    /**
     * Generated
     */

    protected $table = 'file';
    protected $primaryKey ='file_id';
    protected $fillable = ['name', 'file_id'];



}
