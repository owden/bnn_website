<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Websec extends Model {

    /**
     * Generated
     */

    protected $table = 'websec';
    protected $fillable = ['websec_id', 'name', 'class_name'];


    public function webparts() {
        return $this->belongsToMany(\App\Models\Webpart::class, 'webpart', 'websec_id', 'parent_id');
    }

    public function webparts() {
        return $this->hasMany(\App\Models\Webpart::class, 'websec_id', 'websec_id');
    }


}
