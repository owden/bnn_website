<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webcontent extends Model {

    /**
     * Generated
     */

    protected $table = 'webcontent';
    protected $primaryKey ='webcontent_id'; 
    protected $fillable = ['webcontent_id', 'webpart_id', 'content', 'title'];


    public function webcontent() {
        return $this->belongsTo(\App\Models\Webcontent::class, 'webpart_id', 'webcontent_id');
    }

    public function webcontents() {
        return $this->hasMany(\App\Models\Webcontent::class, 'webpart_id', 'webcontent_id');
    }

    public function webgals() {
        return $this->hasMany(\App\Models\Webgal::class, 'webcontent_id', 'webcontent_id');
    }
    
    public function gallery() {
        return $this->hasOne(\App\Models\Webgal::class, 'webcontent_id', 'webcontent_id');
    }


}
