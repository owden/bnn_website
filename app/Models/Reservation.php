<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {

    /**
     * Generated
     */

    protected $table = 'reservation';
    protected $fillable = ['checkin_date', 'checkout_date', 'no_people', 'reservation_id', 'email'];



}
