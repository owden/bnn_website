<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model {

    /**
     * Generated
     */

    protected $table = 'rating';
     protected $primaryKey ='rank_id';
    protected $fillable = ['count', 'rank_id'];



}
