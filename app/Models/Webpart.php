<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webpart extends Model {

    /**
     * Generated
     */

    protected $table = 'webpart';
     protected $primaryKey ='webpart_id'; 
    protected $fillable = ['webpart_id', 'parent_id', 'websec_id', 'name'];


    public function websec() {
        return $this->belongsTo(\App\Models\Websec::class, 'websec_id', 'websec_id');
    }

    public function webpart() {
        return $this->belongsTo(\App\Models\Webpart::class, 'parent_id', 'webpart_id');
    }

    public function websecs() {
        return $this->belongsToMany(\App\Models\Websec::class, 'webpart', 'parent_id', 'websec_id');
    }

    public function webparts() {
        return $this->hasMany(\App\Models\Webpart::class, 'parent_id', 'webpart_id');
    }

}
