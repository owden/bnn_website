<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
       DB::beginTransaction();
        	Schema::create('file', function(Blueprint $table) {
	    $table->increments('file_id');
	    $table->string('name', 500);
            $table->string('path',500);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
        DB::commit();      
//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
	Schema::drop('file');
	DB::commit();
        //
    }
}
