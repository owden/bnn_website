<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::beginTransaction();
        	Schema::create('webcontent', function(Blueprint $table) {
	    $table->increments('webcontent_id');
	    $table->string('content', 500)->nullable()->change();;
	    $table->integer('webpart_id')->unsigned()->nullable();
            $table->string('title',500)->nullable()->change();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
         Schema::table('webcontent', function(Blueprint $table) {
	    $table->foreign('webpart_id')->references('webpart_id')->on('webpart')->onDelete('cascade')->onUpdate('cascade');
	});
    
        DB::commit();
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::beginTransaction();
	Schema::drop('webcontent');
	DB::commit();
        //
    }
}
