<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebgalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         DB::beginTransaction();
        	Schema::create('webgal', function(Blueprint $table) {
	    $table->increments('webgal_id');
	    $table->integer('webcontent_id')->unsigned()->nullable();;
            $table->integer('file_id')->unsigned()->nullable();;
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
        Schema::table('webgal', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	});
         Schema::table('webgal', function(Blueprint $table) {
	    $table->foreign('webcontent_id')->references('webcontent_id')->on('webcontent')->onDelete('cascade')->onUpdate('cascade');
	});
        DB::commit();
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::beginTransaction();
	Schema::drop('webgal');
	DB::commit();
        //
    }
}
