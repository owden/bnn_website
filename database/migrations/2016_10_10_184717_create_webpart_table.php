<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebpartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::beginTransaction();
        	Schema::create('webpart', function(Blueprint $table) {
	    $table->increments('webpart_id');
	    $table->string('name', 500);
	    $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('websec_id')->unsigned()->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
        
         Schema::table('webpart', function(Blueprint $table) {
	    $table->foreign('websec_id')->references('websec_id')->on('websec')->onDelete('cascade')->onUpdate('cascade');
            
             $table->foreign('parent_id')->references('webpart_id')->on('webpart')->onDelete('cascade')->onUpdate('cascade');
	});
        
        DB::commit();
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
	Schema::drop('webpart');
	DB::commit();
        //
    }
}
