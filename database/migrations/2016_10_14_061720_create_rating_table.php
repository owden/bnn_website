<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            DB::beginTransaction();
        	Schema::create('rating', function(Blueprint $table) {
	    $table->increments('rating_id');
            $table->integer('count');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
          DB::commit();
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
	Schema::drop('rating');
	DB::commit();
        //
    }
}
