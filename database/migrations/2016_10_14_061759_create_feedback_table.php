<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::beginTransaction();
        	Schema::create('feedback', function(Blueprint $table) {
	    $table->increments('feedback_id');
            $table->string('name',200);
            $table->string('feedback', 500);
            $table->string('country', 200);
            $table->string('phone_number',14);
            $table->string('email',200);
            $table->string('isread',1)->default('0');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
          DB::commit();    
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
	Schema::drop('feedback');
	DB::commit();
        //
    }
}
