<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            DB::beginTransaction();
        	Schema::create('reservation', function(Blueprint $table) {
	    $table->increments('reservation_id');
            $table->string('firstname',200);
            $table->string('lastname', 200);
            $table->string('other', 200);
            $table->integer('no_people');
            $table->integer('adult');
            $table->string('email',200);
            $table->date('checkin_date');
            $table->date('checkout_date');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
          DB::commit();
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::beginTransaction();
	Schema::drop('reservation');
	DB::commit();
        //
    }
}
